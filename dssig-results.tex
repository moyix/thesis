\section{Results}
\label{res}

The experimental results are given below. We describe the outcome of profiling twenty different applications and present the results of the fuzzing stage. These features are used by the signature generator to find constraints and create a new process scan module for Volatility. Finally, we compare the accuracy of our scanner with other popular scanners.

Throughout, we also consider what our results tell us about the features used by another popular signature (PTFinder's signature for \texttt{EPROCESS}). We find that after fuzzing and profiling, only two of its nine features are resistant to evasion; the remaining invariants are not sufficient to avoid matching random portions of memory.

\subsection{Profiling}
\label{res:profiling}

After profiling the twenty applications described in Section \ref{meth:profile}, we can confirm our hypothesis that some fields are accessed only rarely, if ever. Of the 221 fields in the \texttt{EPROCESS} data structure, 32 were never accessed during the execution of the profiled programs. At the other extreme, 72 were accessed for every application and are thus strong candidates for a process signature. In between are 117 fields that were accessed by some programs, but not others; Figure \ref{accessed_totals} gives a histogram detailing precisely how many programs accessed each field.

Included in the 32 fields that were never accessed are three of the nine used by PTFinder to locate processes in memory dumps; a further four are only accessed by a subset of the programs profiled (the profiling results for the fields used by PTFinder are shown in Figure \ref{ptfinder_fields}). Because the signature used in PTFinder is conjunctive (all of its constraints must be met in order to report a match), and the attacker has complete control over three of the fields used in the signature, \emph{we can conclude that this signature can be trivially evaded}. The features chosen by PTFinder's author did not correspond to those used by the OS, demonstrating that human judgment may not be sufficient to determine what fields are appropriate for use in data structure signatures.

\begin{figure}[t!]
    \centering
    \begin{subfigure}[b]{\textwidth}
        \includegraphics[width=\textwidth]{dssig-figures/accessed_totals.pdf}
        \caption{Number of profiled programs in which \texttt{EPROCESS} fields were accessed. Only fields accessed by all 20 programs provide the strongest assurance for use in a signature.}
        \label{accessed_totals}
    \end{subfigure}

    \begin{subfigure}[b]{\textwidth}
        \includegraphics[width=\textwidth]{dssig-figures/ptfinder_fields.pdf}
        \caption{Access prevalence of fields used by PTFinder's \texttt{EPROCESS} signature. Note that the signature relies on field values never used by the OS, so an attacker can safely change these values to evade the signature.}
        \label{ptfinder_fields}
    \end{subfigure}

    \caption{Access prevalence for \texttt{EPROCESS} for profiled applications.}
    \label{profile_access_hist}
\end{figure}

\begin{figure*}[t!]
    \begin{center}
        \includegraphics[width=5in]{dssig-figures/combined_failhist_new.pdf}
    \end{center}
    \caption[Fuzzing results for \texttt{EPROCESS}]{Fuzzing results for \texttt{EPROCESS}. The y-axis represents the total number of tests for which $\phi$ returned false, indicating that the process was no longer functioning correctly. Higher bars indicate stronger features.}
\label{combined_failhist}
\end{figure*}

\subsection{Fuzzing}
\label{res:fuzz}

We then took the 72 fields identified as always accessed during the profiling stage and fuzzed them using the four different data patterns (zero, random, random primitive, and random aggregate), modifying each field with each pattern five times, for a total of 1,440 distinct tests. The overall number of failed tests for each field is shown in Figure \ref{combined_failhist}. However, this does not provide a full picture of the fuzzing results, as it is also important to note which data patterns caused the OS to fail. It may be acceptable to use a field in a signature even if it is possible to write zeroes to that field, for example, because the constraint could include zero as a special case. We have, therefore, included several sample fields in Table \ref{fuzz_table}, in order to give an idea of what the result data looks like.

We find, as expected, that there are many ``essential'' fields upon which we may base our signature. Five fields failed every attempt at manipulation, and a further 12 failed more than half of the tests (i.e., more than 10). This will give us a set of robust features that is large enough to ensure that the number of false positives found by the signature is minimized.

As in the profiling stage, we also note that these results give us a very strong indication of what fields \emph{not} to choose. Of the 72 fields from profiling, 29 passed every test (their modification did not result in any loss of functionality); these, again, would be a poor basis for a signature as their values can be controlled by an attacker.

\begin{table}[t!]
\centering
\caption[Selected \texttt{EPROCESS} fields and the results of fuzzing them]{Selected \texttt{EPROCESS} fields and the results of fuzzing them. The values indicate the number of times a given test caused $\phi$ to return false, indicating that the OS or program had stopped working correctly. The columns indicate number of OS crashes when testing with the \textbf{Z}ero, \textbf{R}andom, Random \textbf{P}rimitive, and Random \textbf{A}ggregate patterns.}
\label{fuzz_table}
\begin{tabular}{ | l | c | c | c | c | c | }
\hline
\textbf{Field} & \textbf{Z} & \textbf{R} & \textbf{P} & \textbf{A} & \textbf{Total} \\ \hline
ActiveProcessLinks.Flink & 5 & 5 & 5 & 5 & 20 \\ \hline
Pcb.DirectoryTableBase[0] & 5 & 5 & 5 & 5 & 20 \\ \hline
Pcb.ThreadListHead.Flink & 5 & 5 & 5 & 5 & 20 \\ \hline
Token.Value & 5 & 5 & 5 & 3 & 18 \\ \hline
Token.Object & 5 & 5 & 5 & 1 & 16 \\ \hline
VadHint & 5 & 5 & 2 & 0 & 12 \\ \hline
UniqueProcessId & 1 & 5 & 5 & 1 & 12 \\
\hline
\end{tabular}
\end{table}

\subsection{Signature Accuracy}
\label{res:accuracy}

With a list of robust features in hand, we used our signature generator to find constraints on the values of each feature. The field values were collected from 184 processes across the four images in our training set and constraints were inferred using the templates described in Section \ref{meth:siggen}, producing the constraints shown in Table \ref{constraints_table}. The signature generator produced a plugin for Volatility that uses the constraints found to search for \texttt{EPROCESS} instances in memory images.

\begin{table*}[t]
\centering
\caption[Constraints found for ``robust'' fields in the \texttt{EPROCESS} data structure]{Constraints found for ``robust'' fields in the \texttt{EPROCESS} data structure. The operators shown have the same meaning as in C; \% stands for the mod operation, and \& represents bitwise AND. \&\& and \textbar\textbar~ are the boolean operators for ``and'' and ``or'', respectively.}
\label{constraints_table}
\begin{tabular}{ | l | l | }
\hline
\textbf{Field} & \textbf{Constraint} \\
\hline
Pcb.ReadyListHead.Flink & \scriptsize \texttt{val \& 0x80000000 == 0x80000000 \&\& val \% 0x8 == 0} \\
\hline
Pcb.ThreadListHead.Flink & \scriptsize \texttt{val \& 0x80000000 == 0x80000000 \&\& val \% 0x8 == 0} \\
\hline
WorkingSetLock.Count & \scriptsize \texttt{val == 1 \&\& val \& 0x1 == 0x1} \\
\hline
Vm.VmWorkingSetList & \scriptsize \texttt{val \& 0xc0003000 == 0xc0003000 \&\& val \% 0x1000 == 0} \\
\hline
VadRoot & \scriptsize \texttt{val == 0 \textbar\textbar~(val \& 0x80000000 == 0x80000000 \&\& val \% 0x8 == 0)} \\
\hline
Token.Value & \scriptsize \texttt{val \& 0xe0000000 == 0xe0000000} \\
\hline
AddressCreationLock.Count & \scriptsize \texttt{val == 1 \&\& val \& 0x1 == 0x1} \\
\hline
VadHint & \scriptsize \texttt{val == 0 \textbar\textbar~(val \& 0x80000000 == 0x80000000 \&\& val \% 0x8 == 0)} \\
\hline
Token.Object & \scriptsize \texttt{val \& 0xe0000000 == 0xe0000000} \\
\hline
QuotaBlock & \scriptsize \texttt{val \& 0x80000000 == 0x80000000 \&\& val \% 0x8 == 0} \\
\hline
ObjectTable & \scriptsize \texttt{val == 0 \textbar\textbar~(val \& 0xe0000000 == 0xe0000000 \&\& val \% 0x8 == 0)} \\
\hline
GrantedAccess & \scriptsize \texttt{val \& 0x1f07fb == 0x1f07fb} \\
\hline
ActiveProcessLinks.Flink & \scriptsize \texttt{val \& 0x80000000 == 0x80000000 \&\& val \% 0x8 == 0} \\
\hline
Peb & \scriptsize \texttt{val == 0 \textbar\textbar~(val \& 0x7ffd0000 == 0x7ffd0000 \&\& val \% 0x1000 == 0)} \\
\hline
Pcb.DirectoryTableBase.0 & \scriptsize \texttt{val \% 0x20 == 0} \\
\hline
\end{tabular}
\end{table*}

We then evaluated the accuracy of three process scanners: our own automatically generated scanner, Volatility's \texttt{psscan2} module, and PTFinder \cite{Schuster:2006eb}. Using each scanner, we searched for instances of the \texttt{EPROCESS} data structure in the three memory images listed in Section \ref{meth:siggen}. The output of each tool was compared against the OS's list of running processes (found by walking the linked list of process data structures using Volatility's \texttt{pslist} module). In the case of the non-NIST image, we also checked for the presence of our hidden process, which was not visible in the standard OS process list.

We found that all three scanners had equal detection performance on the NIST images and found every process data structure with no false positives. However, only our own scanner was able to detect the hidden process in the third image, demonstrating that an attacker could potentially evade both psscan2 and PTFinder with minimal effort. We believe our signature will also prove resistant to evasion against real-world attackers, as the features it uses are demonstrably difficult for an attacker to alter.

Aside from the active processes in the images, we also noted some discrepancies between the three scanners with respect to terminated processes whose \texttt{EPROCESS} structure was still in memory and had not yet been overwritten. Although PTFinder and psscan2 were vulnerable to the evasion by our custom malware, they also found these terminated processes, which our scanner missed.

As terminated processes could be of forensic interest, we checked whether there was some subset of our ``robust'' features that would find such processes without introducing false positives. By modifying our scanner to report the result of each constraint for the terminated processes, we found that Windows appears to zero the \texttt{Token.Object} and \texttt{Token.Value} fields, which refer to the process's security token, when the process exits. Once we removed these constraints from our signature, we were able to find the terminated processes reported by other tools without introducing false positives. We note that our scanner remains resistant to evasions, as the remaining fields are all robust. The terminated processes demonstrate the importance of generating signatures from a training set that represents the full range of objects one wishes to detect.

%The terminated processes demonstrate the importance for the signature generation step to learn constraints from a collection of data structures representative of the complete set of objects one wishes to detect.
