\section{Motivation}
\label{virtuoso:intro}

As kernel-level malware has become both more common and more
sophisticated, some researchers have advocated protecting insecure
commodity operating systems through the use of virtualization-based
security \cite{payne:2008,garfinkel:2003b,laureano:2004,joshi:2005a}. By
moving protection below the level of the OS, these solutions aim to
provide many traditional security services such as intrusion detection
and policy enforcement without fear of subversion by malicious code
running on the system. At the same time, because the code base of the
hypervisor is small, there is some hope that it can be verified and
provide secure isolation for the virtual machines it supervises.

This enhanced security and isolation comes at a cost, however. Because
the guest operating system is untrusted, information about its state
can not be reliably obtained by calling standard APIs within the guest.
Instead, security tools must use \emph{introspection} to retrieve
information about the current state of the guest OS by examining the
physical memory of the running virtual machine and using detailed
knowledge of the operating system's algorithms and data structures to
rebuild higher level information such as lists of running processes,
open files, and active network connections. Even if the guest OS is
compromised, the tools using introspection will continue to report
accurate results, so long as the underlying internal data structures
used by the operating system are intact.

The creation and maintenance of introspection tools, therefore, is
dependent on detailed, up-to-date information about the internal
workings of commodity operating systems. Even for systems where the
source code to the kernel is available, acquiring this knowledge can be
a daunting task; when source is not available, prolonged effort by a
skilled reverse engineer may be required. Moreover, the time and effort
spent divining the internals of a particular version of an operating
system may not be applicable to future versions. This problem of extracting
high-level semantic information from low-level data sources, known as the
\emph{semantic gap}, presents a significant barrier to the development 
and widespread deployment of virtualization security products.

The fact that creating introspection tools by hand is difficult has
multiple security implications. Security professionals and system
administrators rely upon the output of hand-built introspection tools;
at the same time, these tools are fragile and often cease to function
upon application of operating system patches. Currently, therefore, one
must often choose between keeping an up-to-date system and maintaining a
working set of introspections. Moreover, hand-built introspection tools
may introduce opportunities for attackers to evade security tools: to
the extent that introspection must replicate the behavior of in-guest
functionality, the developer must have an accurate model of the OS's
inner workings. Garfinkel \cite{Garfinkel:2003} has demonstrated, with
numerous examples drawn from his experience with system call monitors,
that any divergence between reality and the model assumed by a security
program can provide opportunities for evasion.

In this chapter, we both ease the burden of maintaining
introspection-based security tools and make them more secure by
providing techniques to \emph{automatically} create programs that can
extract security-relevant information from outside the guest virtual
machine. Our fundamental insight is that it is typically trivial to
write programs inside the guest OS that compute the desired information
by querying the built-in APIs. For example, to get the ID of the
currently running process in Linux, one need only call
\texttt{getpid()}; by contrast, implementing the same functionality from
outside the virtual machine depends on intimate knowledge of the layout
and location of kernel structures such as the \texttt{task\_struct}, as
well as information about the layout of kernel memory and global
variables. Our solution, named Virtuoso, takes advantage of the OS's own
knowledge (as encoded by the instructions it executes in response to a
given query) to learn how to perform OS introspections. For example,
using Virtuoso, a programmer can write a program that calls
\texttt{getpid()} and let Virtuoso trace the execution of this program,
automatically identify the instructions necessary for finding the
currently running process, and finally generate the corresponding
introspection code.

Virtuoso takes into account complexities that arise from translating a
program into a form that can run outside of its native environment. It
must be able to extract the program code, and its dependencies; to do
this, we use a dynamic analysis that captures \emph{all} the code
executed while the program is running. At the same time, Virtuoso must
be able to distinguish between the introspection code and unrelated
system activity, a challenge that we overcome by making use of dynamic
slicing \cite{Korel:1988} to identify the exact set of instructions
required to compute the introspection.  Finally, the use of dynamic
analysis means that a single execution of the program may not cover all
paths required for reliable re-execution.  To ensure that our generated
programs are reliable, we introduce a method for merging multiple
dynamic traces into a single coherent program and show that its use
allows our generated programs to achieve high reliability.
