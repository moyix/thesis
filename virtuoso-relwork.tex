\section{Related Work}
\label{sec:virtuosorelwork}

Most closely related to our own techniques are two recent works which
perform extraction of binary code for security purposes. Binary Code
Reutilization (BCR) \cite{Caballero:2010} and Inspector Gadget
\cite{Kolbitsch:2010} focus on the problem of extracting subsets of
binary code, particularly in order to reuse pieces of malware
functionality (such as domain name generation or encryption functions).
While the former primarily uses static analysis and recursively descends
into function calls, the latter adopts a dynamic approach similar to our
own. Inspector Gadget performs a dynamic slice on a log collected from a
single run of the malicious code and changes half-covered conditional
branches into unconditional jumps (the authors do not discuss the
possible correctness issues that may arise from this strategy, nor do
they say whether their system can combine multiple dynamic traces into a
single program). Both systems are more limited in scope than Virtuoso
and do not consider kernel code. They make extensive use of domain
knowledge about the target operating system, and would therefore be
difficult to adapt to new platforms. By contrast, Virtuoso uses no
domain knowledge aside from understanding the x86 platform, and performs
\emph{whole-system} binary code extraction.

More generally, virtual machine introspection has played an integral
role in a number of recent security designs. Garfinkel and Rosenblum
\cite{garfinkel:2003b} first proposed the idea of performing intrusion
detection from outside the virtual machine.  Their system introspects on
Linux guests to detect certain kinds of attacks; higher level
information is reconstructed through the use of the \texttt{crash}
utility \cite{crash}. Since then, other researchers have proposed using
virtualized environments for tasks such as malware analysis
\cite{Dinaburg:2008, jiang:2007} and secure application-layer firewalls
\cite{Srivastava:2008zp}; these systems all make heavy use of virtual
machine introspection and therefore must maintain detailed and accurate
information on the internals of the operating systems on which they
introspect.

Introspection can also be useful in contexts outside of traditional
virtualization security. Petroni et al. \cite{petroni:2004a} presented a
system called Copilot that polls physical memory from a PCI card to
detect intrusions; such detection needs introspection to be useful.
Malware analysis platforms that run samples in a sandboxed environment,
such as CWSandbox \cite{Willems2007} and Anubis \cite{bayer09:scalable},
can also benefit from introspection: by extracting high-level
information about the state of the system as the malware runs, more
meaningful descriptions of its behavior can be generated. At the same
time, this introspection must be secure and unobtrusive, as the guest OS
is guaranteed to be compromised. Our work can help provide higher-level
semantic information to such systems.

Originally proposed by Korel and Laski \cite{Korel:1988}, the dynamic
slicing at the heart of our algorithm has been used in several other
recent security tools, such as HookMap \cite{Wang:2008} and K-Tracer
\cite{Lanzi:2009}. The former makes use of this technique to identify
memory locations in the kernel that could be used by a rootkit to divert
control flow, while the latter uses a combination of dynamic backward and
forward slicing (also known as ``chopping'') to analyze the behavior of
kernel malware with respect to sensitive data such as process listings.
Additionally, Kolbitsch et al. \cite{Kolbitsch:2009} described a malware
detection system that uses dynamic slicing to extract the code necessary
to relate return values and arguments between system calls.
