\chapter{Introduction}

\section{Motivation and Goals}

We are surrounded by complex, closed systems every day. These deployed
systems are fragile and were often not created with security in mind,
but are expensive to replace and will likely be with us for decades.
They often include software whose manufacturers have long since gone out
of business, and for which no detailed documentation still exists. Even
in cases where closed-source software still has vendor support,
attitudes toward intellectual property and trade secrets may prevent a
vendor from divulging the internal workings of their software.

Given the difficulty of getting code creators to willingly hand over
implementation details, how can such systems be effectively protected?
The inner workings of a system are of vital interest to security
systems: the difference between a simple bug and a major vulnerability
quote often hinges on some minor detail of implementation; forensics
software must understand undocumented formats and algorithms in order to
reconstruct a picture of a suspect's activity that will hold up in
court; and intrusion detection and prevention systems must understand
the intentionally obscure techniques malware uses to insinuate itself
into a system and carry out attacks.

Beyond merely protecting existing systems, there are other security
motivations for developing a detailed understanding of the
implementation details of closed-source software. It may often be
necessary to understand the undocumented behavior of some code without
cooperation from its author. In the case of malware, this point is
obvious, but even for benign software we may need to peer into its
internals to verify that it works as claimed. For example, in 2013,
researchers challenged~\cite{imessage} Apple's claims that messages sent
by its iMessage chat client could not be read by anyone but the intended
recipient (including Apple); in refuting this claim researchers had to
reverse engineer the iMessage client. Even more disturbingly, by 
reverse engineering the TOM-Skype client (a special version of Skype
distributed in China) Knockel et al.~\cite{Knockel2011} discovered a
secret list of words that would trigger censorship and surveillance. The
question of whether the software one uses is acting in its user's
interests can be critical to one's safety and security and can often
only be answered through reverse engineering.

For these reasons, in order to protect deployed systems and understand
what the software we use is doing, we need automated techniques that can
understand software based only on binary artifacts. Although there has
been a great deal of research in the past decade on binary software
analysis for vulnerability discovery, less work has been done on
automatically inferring the details of large scale systems such as
operating systems. For such systems, techniques such as static analysis
often break down due to the large amount of code and certain constructs
prominent in systems code such as indirection through function pointers.

In this work, we seek to develop techniques that can elucidate the
internal workings and assumptions of modern operating systems and
programs. In particular, our focus will be on techniques which allow
security tools to effectively monitor the runtime state and behavior of
closed-source systems in order to provide services such as anti-virus
monitoring, intrusion detection, and rootkit detection. Our tools of
choice will be dynamic analyses, for these typically scale better to
large code bases than static analysis, and will use the data handled and
output by the system as a guide to code analysis, as it is ultimately
data, not code, which determines the state of the system.

\section{Thesis Overview}

Security systems often cannot rely on the assumption that the operating
system has not been tampered with; because of this, they must come to
their own understanding of the high-level state of the system
independent of the provided APIs. To achieve this goal, this thesis
presents \textbf{novel dynamic analysis techniques for automatically
understanding closed-source systems in order to develop better
protections}. Our first three chapters focus on the application of such
techniques to intrusion detection systems depicted in
Figure~\ref{fig:intro}, where an external security monitor must
understand internal details of program and operating system
implementation in order to detect threats. Our final chapter discusses a
more general approach to program understanding, in which dynamic
features of execution are used to classify unknown behaviors in
benign and malicious code.

\begin{figure}[ht]
    \includegraphics[width=\textwidth]{intro-figures/NoTrustIDS.pdf}
    \caption{The architecture of an intrusion detection system that
        attempts to place as little trust in the operating system as
        possible. Because the OS cannot be trusted, the security monitor
        must reproduce and understand implementation details that are
        not normally made public.}
    \label{fig:intro}
\end{figure}

In Chapter~\ref{chap:dssig}, we examine the use of \emph{data structure
signatures} for detecting hidden and unlinked structures in operating
system memory. Such signatures rely on the fact that data structure
instances typically have \emph{invariants} on their fields that allow
them to be located by looking for patterns in memory. These invariants,
however, must be enforced by the operating system, or attackers will
still be able to hide kernel objects by manipulating the structure's
fields in ways that violate the supposed ``invariant''. Thus, to place
memory scanning techniques on a firm footing and develop \emph{robust
signatures}, we introduce a dynamic analysis based on memory access profiling
and fuzzing that probes data structure fields in order to detect which
invariants are actually enforced and can therefore be safely used in
signatures. Armed with such signatures, the security monitor in
Figure~\ref{fig:intro} can compare the operating system's self-reported view of
the system and what actually exists in memory.

Rather than scanning memory for relevant objects, intrusion detection
systems may instead traverse the data structures that represent the
system state in the same way that the operating system or program
would---i.e., by starting from some global variables, following pointers
that make up linked list or tree structures, and so on. This method,
however, relies on much more intimate knowledge of the structures and
algorithms used by the system and the burden of reverse engineering is
correspondingly higher (this mismatch between the high-level semantic
view needed by a security monitor and a low-level view of e.g. physical
memory is often referred to as the \emph{semantic gap}). In
Chapter~\ref{chap:virtuoso}, we introduce Virtuoso, a system which
automatically extracts small subprograms from a larger operating system
or application that implement the work traversing the program's data
structures and interpreting its internal state. This is done by
identifying some API that outputs the desired data and then using
\emph{dynamic slicing} and \emph{trace merging} to identify and extract
the code responsible for computing that data. In essence, Virtuoso
extracts and encapsulates a program or operating system's own mechanisms
for interpreting its internal state and allows them to be used
independent of the original program. In the IDS shown in
Figure~\ref{fig:intro}, these extracted programs can be used for
periodic object traversal.

Passive monitoring is only half the story for an intrusion detection
system, however. Real-world security systems also need to support
\emph{active monitoring}, where events that occur in the system trigger
notifications to the security monitor. Such events include file and URL
accesses, log messages, cryptographic key generation (so that encrypted
network traffic can be monitored), and so on. In order to support this
use case, we need ways of quickly locating the code in a system that is
responsible for carrying out the actions we are interested in
monitoring. In Chapter~\ref{chap:tzb} we describe Tappan Zee (North)
Bridge, or TZB, a system that leverages the fact that the data
associated with a security event will be written to and read from memory
when the event occurs; thus, by monitoring all memory accesses made in
the system and searching through these streams for data associated with
the event in question, we can locate the code location where a hook
should be placed. This information can the be provided to provide the
final piece of the intrusion detection system shown in
Figure~\ref{fig:intro}, the active monitoring component.

Finally, in Chapter~\ref{chap:mal} we examine a new technique for for
detecting code with similar functionality by looking at the content
handled by functions during a dynamic trace. The problem of identifying
functionality in an unknown, binary code base is one that is relevant to
several security problems: malware analysts need to quickly determine
the capabilities of previously unseen malware and relate them to
existing samples; vulnerability researchers often need to quickly zero
in on portions of code that are prone to vulnerabilities such as input
parsing; and reverse engineering efforts of all sorts can benefit from a
high-level view of what different sections of code are likely to do
before performing a detailed analysis of any one part.

We conclude in Chapter~\ref{chap:conclusion} by examining a number of
broad, open problems in this area and considering what changes new
systems might incorporate in order to make themselves more amenable to
the kind of detailed internal inspection needed for security monitoring.
