\section{Architecture}
\label{arch}

\begin{figure*}[t!]
\centering
\includegraphics[width=5in]{dssig-figures/profiling.pdf}
\caption[The profiling stage of our signature generation system]{The profiling stage of our signature generation system. As the OS executes, accesses to the target data structure are logged. These logs are then combined with knowledge of the field layout in the structure to produce an access profile.}
\label{profile_arch}
\end{figure*}

Our system architecture generates signatures using a three step process. We first \emph{profile} the data structure we wish to model to determine which fields are most commonly accessed by the operating system (Section \ref{arch:profiling}). This is done to narrow the domain of the data that we must test in the fuzzing stage: if a field is never accessed in the course of the normal operation of the OS, it is safe to assume that it can be modified without adversely affecting OS functionality. Next, the most frequently accessed fields are \emph{fuzzed} (Section \ref{arch:fuzzing}) to determine which can be modified without causing a crash or otherwise preventing the structure from serving its intended purpose. Finally, we collect known-good instances of the data structure, and build a signature based on these instances that depends only on the features that could not be safely modified during fuzzing (Section \ref{arch:siggen}).

Profiling and fuzzing are both essentially forms of \emph{feature selection}. Each tests features of the data structure to determine their suitability for use in a signature. Features that are unused by the operating system or are modifiable without negative consequences are assumed to be under the control of the attacker and eliminated from consideration. Including such weak features would allow an attacker to introduce \emph{false negatives} by violating the constraints of a signature that used them, in the same way that a polymorphic virus evades an overly specific antivirus signature. At the other end of the spectrum, if too few features remain at the end of feature selection, the resulting signature may not be specific enough and may match random data, creating \emph{false positives}.

The profiling and fuzzing stages are implemented using the Xen hypervisor \cite{barham:2003} and VMware Server \cite{VMware}, respectively. Because profiling requires the ability to monitor memory access, we chose to use Xen, which is open source and allowed us to make the necessary changes to the hypervisor to support this monitoring. However, Xen lacks the ability to save and restore system snapshots, a feature needed for reliable fuzz testing, so we use VMware Server for this stage. Also, because VMware's snapshots save the contents of physical memory to disk, we were able to easily modify the memory of the guest OS by altering the on-disk snapshot file.

\subsection{Data Structure Profiling}
\label{arch:profiling}

In the profiling stage (shown in Figure \ref{profile_arch}), we attempt to determine which structure fields are most commonly accessed by the operating system during normal operation. Fields which are accessed by the OS frequently are stronger candidates for use in a signature because it is more likely that correct behavior of the system depends upon them. By contrast, fields which are rarely accessed are most likely available to the attacker for arbitrary modification; if the OS never accesses a particular field in the data structure, its value cannot influence the flow of execution.

In our implementation, we make use of a modified Xen hypervisor and the ``stealth breakpoint'' technique described by Vasudevan and Yerraballi \cite{Vasudevan:2005ph} to profile access to the data structure. Stealth breakpoints on memory regions work by marking the memory page that contains the data to be monitored as ``not present'' by clearing the Present bit in the corresponding page table entry. When the guest OS makes any access to the page, the page fault handler is triggered, an event which can be caught by the hypervisor. The hypervisor then logs the virtual address that was accessed (available in the \texttt{CR2} register), emulates the instruction that caused the fault, and allows the guest to continue. These logs can later be examined to determine what fields were accessed, and how often.

For example, to monitor the fields of the Windows \texttt{EPROCESS} data structure, we launch a process and determine the address in memory of the structure. We then instruct the hypervisor to log all access to that page, and then allow the process to run for some time. Finally, the logs are examined and matched against the structure's definition to determine how often individual fields were read or written. This process is repeated using several different applications; only the fields that are accessed during the execution of \emph{every} program will be used as input for the fuzzing stage.

We note in passing that determining the precise field accessed requires access to the data structure's definition. On open source operating systems, this information is easy to come by, but for closed source OSes such as Windows it may be more difficult to obtain. For our implementation, which targets Windows XP, we used the debugging symbols provided by Microsoft; these symbols include structure definitions for many kernel structures, including \texttt{EPROCESS}.

\subsection{Fuzzing}
\label{arch:fuzzing}

Although a field that is accessed frequently is a stronger candidate than one which is never accessed, this condition alone is not sufficient to identify truly robust features for use in signatures. For example, the operating system may update a field representing a performance counter quite frequently, but its value is not significant to the correct operation of the OS. Thus, to be confident that a signature based on a particular field will be robust against evasion attacks, we must ensure that the field cannot be arbitrarily modified.

The actual fuzzing (shown in Figure \ref{fuzz_arch}) is done by running the target operating system inside VMware Server \cite{VMware}. As in the profiling stage, we first create a valid instance of the data structure. Next, the state of the guest VM is saved so that it can be easily restored after each test. For each field, we then replace its contents with test data from one of several classes:

\begin{enumerate}
\item \textbf{Zero}: null bytes. This is used because zero is often a significant special case; e.g., many functions check if a pointer is NULL before dereferencing.
\item \textbf{Random}: $n$ random bytes from a uniform distribution, where $n$ is the size of the field.
\item \textbf{Random primitive type}: a random value appropriate to the given primitive type. In particular, pointer fields are fuzzed using valid pointers to kernel memory.
\item \textbf{Random aggregate type}: a random value appropriate to the given aggregate type (i.e., structure). Embedded structures are replaced by other valid instances of that structure, and pointers to structures of a given type are replaced by pointers to that same type. Currently implemented as a random choice of value from that field in other instances of the target data structure.
\end{enumerate}

After the data is modified, we resume the guest VM and monitor the operating system to observe the effects of our modifications. To determine whether our modification was harmful, we must construct a test, $\phi$, which examines the guest and checks if the OS is still working and the functionality associated with the target data structure instance is still intact.

\begin{figure}
\centering
\includegraphics[width=3in]{dssig-figures/fuzzing.pdf}
\caption[The architecture of the fuzzing stage]{The architecture of the fuzzing stage. Starting from some baseline state, a test pattern is written into a field in the target data structure in the memory of the virtual machine. The VM is then resumed and tested to see if its functionality is intact. If so, the modification was not harmful and we consider the field a weaker candidate for a signature.}
\label{fuzz_arch}
\end{figure}

For a process data structure, $\phi$ could be a test that first checks to see if the OS is running, and then determines whether the associated program is still running. This check may be simpler to program if the behavior of the application in question is well-known. In our experiments (described in Section \ref{meth:fuzzing}), we used an application we had written that performed some simple functions such as creating a file on the hard drive. This allowed $\phi$ to check if the program had continued to run successfully by simply testing for the existence of the file created by the program.

To actually inject the data, we pause the virtual machine, which (in VMware) writes the memory out to a file. We then use the memory analysis framework Volatility \cite{volatility} (which we modified to support writing to the image) to locate the target instance and modify the appropriate field with our random data. Volatility was ideal for this purpose, because it has the ability to locate a number of kernel data structures in images of Windows memory and provides a way to access the data inside the structures by field name. The modifications to allow writing to the image (a feature not normally supported by Volatility, as it is primarily a forensics tool) required 303 lines of additional code.

Finally, we resume the virtual machine and check to see if $\phi$ indicates the system is still functioning correctly after some time interval. This interval is currently set to 30 seconds to allow time for the VM to resume and any crashes to occur. Software engineering studies \cite{Narayanasamy:2005rr} have found that crashes typically occur within an average of one million instructions from the occurrence of memory corruption; thus, given current CPU speeds, it is reasonable to assume that this 30 second delay will usually be sufficient to determine whether the alteration was harmful to program functionality. The result of the test is logged, and we restore the saved virtual machine state before running the next test. Any fields whose modification consistently caused $\phi$ to indicate failure are used to generate a signature for the data structure.

\begin{figure}[t!]
\centering
\includegraphics[width=3in]{dssig-figures/scanner_code.pdf}
\caption[Two sample constraints found by our signature generator]{Two sample constraints found by our signature generator. If all constraints match for a given data buffer, the plugin will report that the corresponding location in memory contains an \texttt{EPROCESS} instance.}
\label{siggen_output}
\end{figure}

\subsection{Signature Generation}
\label{arch:siggen}

The final signature generation step is performed using a simplified version of dynamic invariant detection \cite{ErnstPGMPTX2007}. For each field identified by the feature selection as robust, we first gather a large number of representative values from all instances of the target data structure in our corpus of memory images. Then, for each field, we test several constraint templates to see if any produce invariants that apply to all known values of that field. The templates checked are:

\begin{itemize}
\item \textbf{Zero subset}: check if there is a subset of the values that is zero. If so, ignore these values for the remaining checks.
\item \textbf{Constant}: check if the field takes on a constant value.
\item \textbf{Bitwise AND}: check if performing a bitwise AND of all values results in a non-zero value. This effectively checks whether all values have any bits in common.
\item \textbf{Alignment}: check if there is a power of two (other than 1) on which all values are aligned.
\end{itemize}

First, because many fields use zero as a special case to indicate that the field is not in use, we check if any of the instances are zero, and then remove these from the set to be examined. Constraints are then inferred on the remaining fields, and zero will be included as a disjunctive (OR) case in the final signature. The other templates will produce conjunctive constraints on the non-zero field values.

The \emph{constant} template determines whether a field always takes on a particular value. This is useful, for example, for data structures that have a ``magic'' number that identifies them uniquely. Because the features that are used for signature generation are known to be robust (as these were the selection criteria described in Sections \ref{arch:profiling} and \ref{arch:fuzzing}), we can have some confidence that the operating system performs sanity checking on such constant values.

The two remaining tests are particularly useful for finding constraints on pointer values. The \emph{bitwise AND} test simply performs a bitwise AND of all values observed. In many operating systems, kernel memory resides in a specific portion of virtual address space, such as (in Windows) the upper 2 gigabytes. One can determine if a 32-bit pointer always points to kernel memory, then, by simply checking that the highest bit is set.

Finally, the \emph{alignment} test attempts to find a natural alignment for all values. As an optimization, many OS memory managers allocate memory aligned on a natural processor boundary, such as eight bytes. As a result, most pointers to kernel objects will likewise have some natural alignment that we can discover and use as a constraint.

Our signature generator takes as input a comma-separated file, where each row gives the field name and a list of values observed for that field. For each field, it applies the constraint templates to the values listed and determines a boolean expression that is true for every value. It then outputs a plugin for Volatility, written in Python, that can be used to scan for the target data structure in a memory image. An excerpt of the plugin generated to search for instances of the \texttt{EPROCESS} data structure is given in Figure~\ref{siggen_output}.

The signature generation mechanism produces extremely robust results in practice: as we describe in Section \ref{res:accuracy}, the signature we generated for Windows process data structures found all instances of the data structure in memory with no false positives or negatives. Should this technique prove insufficient for some data structure, however (for example, if only a few features are robust enough to use in a signature), more heavyweight techniques such as dynamic heap type inference \cite{Polishchuk:2007cr} could be used.

\subsection{Discussion}
\label{arch:discussion}

Our experiments (described in Section \ref{res}) show that these techniques can be used to derive highly accurate signatures for kernel data structures that are simultaneously difficult to evade. There are, however, certain drawbacks to using probabilistic methods such as dynamic analysis and fuzz testing. In particular, both techniques may suffer from \emph{coverage} problems. In the profiling stage, it is highly unlikely that every field used by the operating system will actually be accessed; there are many fields that may only be used in special cases. Likewise, during fuzzing, it is possible that although the operating system did not crash during the 30 seconds of testing, it might fail later on, or in some special circumstances. 

In both of these cases, however, we note that these omissions will only cause us to ignore potentially robust features, rather than accidentally including weak ones. Moreover, from an attacker's point of view, the malware need not work perfectly, or run in every special case: sacrificing correct operation in a tiny fraction of configurations may be worth the increased stealth afforded by modifying these fields. Thus, a short time interval for testing is \emph{conservative}: it will never cause a weak feature to be used in a signature, as only features whose modification consistently causes OS crashes form the basis of signatures. However, it may cause fields to be eliminated that would, in fact, have been acceptable to use in a signature. If too many fields are eliminated, the resulting signature may match random data in memory, creating false positives. In any case, this limitation is easily overcome by increasing the amount of time the fuzzer waits before testing the OS functionality, or by exercising the guest OS more strenuously during that time period.

However, there are some coverage issues that could result in weak signatures. Because fuzzing is a dynamic process, it is possible to only inject a subset of values that causes the OS to crash, while there exists some other set of values that can be used without any negative effects. In this case, we may conclude that a given feature is robust when in fact the attacker can modify it at will. For most fields it is not practical to test every possible value (for example, assuming each test takes only five seconds, it would still require over 680 years to exhaustively test a 32-bit integer). In Section \ref{future}, we will   consider future enhancements to the fuzzing stage that may improve coverage.

Finally, we note that although the features selected using our method are likely to be difficult to modify, there is no guarantee that they will be usable in a signature. For example, although our testing found that the field containing the process ID is difficult to modify, it could still be any value, and examining a large number of process IDs will not turn up any constraints on the value. In practice, though, we found that most of the ``robust'' features identified were fairly simple to incorporate into a signature, and we expect that this will be true for most data structures.
