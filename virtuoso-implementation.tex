\section{Implementation}
\label{sec:impl}

Having given a high-level view of our system, we now turn to the details
of its implementation. Virtuoso's trace logging portion is built on top
of QEMU \cite{Bellard:2005}, a fast emulator and binary translator.  Our
Trace Analyzer is written in Python, and consists of 1,843
SLOC;\footnote{Source line counts were generated using David A.
Wheeler's `SLOCCount'.} the majority of this code consists of the data
flow transfer functions for each QEMU micro-instruction. Finally, the
Instruction Translator and runtime environment are 431 and 1,095 lines
of Python code, respectively. These components, and their logical
relationship, are shown in Figure \ref{running_example} on page
\pageref{running_example}. We will discuss in detail the implementation
of each component in this section.

\subsection{Trace Logging}
\label{sec:impl:subsec:logger}

Our trace logger, which is shown on the left in Figure
\ref{running_example}, is a modified version of QEMU. QEMU works by
dynamically translating each guest instruction to a series of operations
in a micro-instruction language.  The micro-instructions are implemented
by small C functions, which are compiled to host-level code and chained
together to emulate the guest code on the host.

To enable logging, we inserted small logging statements into each
micro-instruction's implementation. These logging statements record the
current operation, along with its operands and any dynamic information
needed to enable the data flow analysis (see Section
\ref{sec:design:subsec:ta} for more details). Thus, when the guest code
is translated by QEMU, the generated code will be interleaved with
logging functions that record the micro-instructions executed. This
interleaving is necessary because accurate logging depends on dynamic
information that will only be available once the previous instruction
has finished executing.

\begin{figure}[tb]
    \centering
	\includegraphics[width=3in]{virtuoso-figures/uop.pdf}
    \caption[An example of an x86 instruction and the corresponding
    logged micro-instructions]{An example of an x86 instruction and the corresponding
logged micro-instructions. The implicit pointer dereference has been
made explicit, and the referenced address has been recorded to enable
data flow analysis.}
	\label{micro_op}
\end{figure}

Our logging functions record the current micro-operation, its
parameters, and concrete \emph{physical} memory addresses for each load
and store instruction. Using the physical address allows data flow to be
reconstructed even when multiple virtual addresses reference the same
physical memory (this often occurs when a buffer is shared between a
user application and the kernel, for example). Finally, we record the
page table dependencies for each memory operation. This is necessary
because each memory load or store implicitly depends on the addressing
structures used: if another instruction in the trace has modified the
virtual to physical address map, that instruction must also be included
in order for the resulting program to function correctly. By recording
the address of the page directory and page table entries used in
calculating the address of the load or store, our dynamic slicing
algorithm (described in Section \ref{sec:impl:subsec:dynslice}) can
automatically determine what page table-manipulating code must also be
included.

In Figure~\ref{micro_op}, we give an example of an x86 instruction and
the corresponding QEMU micro-operations that are recorded by the Trace
Logger. The x86 instruction is decomposed into a series of simpler
operations: first, a memory address is computed by taking the value in
the {\tt EBP} register and adding {\tt 0x10} to it. Next, the {\tt
  LDL} micro-op retrieves the value from memory at that address and
stores it in the temporary register {\tt T0}. Along with the micro-op,
the logger records the address the memory was read from to capture any
data flow dependencies. Finally, the value read is transferred to the
{\tt EDI} register.

\begin{figure}[t!]
\centering
\fbox{
\includegraphics[width=3in]{virtuoso-figures/vmnotify.pdf}
}
\caption[In-guest code that signals the Trace Logger to begin tracing]{In-guest code that signals the Trace Logger to begin tracing.
  Code that simply saves and restores clobbered registers has been
  omitted.}
\label{vmnotify}
\end{figure}

These micro-instructions provide a natural IR for our analyzer. Not
only are QEMU micro-ops simple to log, they also satisfy the requirement
of performing relatively simple operations with few side effects. This
greatly simplifies the task of tracking data flow, as the data defined
and used by each micro-instruction is generally easy to understand.

Inside the guest, the \emph{training program} (described in Section
\ref{sec:design:subsec:introprog}) must be able to signal the Trace
Logger and inform it of the beginning and end of the introspection
operation, as well as the location of the buffer where the output has
been placed. Our implementation accomplishes this by co-opting the x86
{\tt CPUID} instruction. When a magic value is placed in the {\tt EBX}
register, our modified QEMU will interpret {\tt CPUID} as a signal to
start or stop logging, and will record the values of {\tt ECX} and {\tt
EDX} as the buffer's start and size, respectively. This allows us to
bound our trace so that it includes only the operations relevant to
introspection, and also provides the Trace Analyzer with the locations
of the input and output buffers. The in-guest code that implements the
notification is given in Figure \ref{vmnotify}.

\subsection{Preprocessing}
\label{sec:impl:subsec:preprocessing}

As discussed in Section \ref{sec:design:subsec:ta}, the trace is
preprocessed before slicing and translation in order to remove hardware
interrupts and replace calls to memory-allocating functions with
function summaries (this is part of the analysis phase, i.e., the middle
portion of Figure \ref{running_example}). Filtering interrupts has two
benefits: it reduces the amount of code we need to analyze, and it
eliminates many accesses to hardware devices (which may not be available
at run time). Replacing memory allocation functions (such as malloc and
realloc), while not necessary in theory, is currently required by our
implementation, as portions of the OS-provided memory allocation
facilities are carried out in the page fault handler, and we do not
support hardware exceptions in our runtime environment.

Interrupt filtering is performed in the obvious way, by matching up
pairs of interrupts (which are logged by the Trace Logger) and
\texttt{iret} instructions (which are used in the x86 architecture to
return from an interrupt). Because interrupts may be nested arbitrarily
deep, we use a counter to ensure that we have matched a given
\texttt{iret} with the appropriate interrupt. Once the outermost
interrupt and \texttt{iret} have been identified, they can be excised
from the trace.

There are two exceptions to interrupt filtering, however. First,
\emph{software interrupts} (i.e., those triggered by an \texttt{int}
instruction) are part of the code of the program and are treated in much
the same way as a standard function call. Second, the x86 architecture,
starting with the Pentium, includes an Advanced Programmable Interrupt
Controller (APIC) unit, which gives the operating system more control
over how and when it receives interrupts. Among the capabilities
provided by the APIC is the ability to send interrupts to other
processors (including itself). When combined with the ability to defer
interrupts by setting the Task Priority Register (TPR) on the APIC, this
provides a mechanism that can be used by the OS to perform
\emph{asynchronous procedure calls}. Because these are effectively
software interrupts (they originate in OS code), we include them in our
analysis and in the generated program.

Finally, we provide replacements for the memory allocation functions of
some operating systems. To do so, we make use of three pieces of domain
knowledge about the OS being analyzed: the virtual address of the
function, the number of argument bytes (to enable callee-cleanup, if the
calling convention requires it), and the position of the ``size''
argument relative to the stack. With this information, one can edit the
trace and replace a call to (e.g.) malloc with a summary that allocates
memory on the host. A summary for RtlAllocateHeap on Windows is shown in
Figure \ref{mallocsum}. We currently replace
\texttt{RtlAllocateHeap}/\texttt{RtlFreeHeap}\cite{rtlallocheap} on
Windows, and
\texttt{malloc}/\texttt{realloc}/\texttt{calloc}/\texttt{free} on Linux.
In our testing, programs generated for the Haiku operating system did
not need malloc summaries.

\begin{figure}[tb]
    \centering
	\includegraphics[width=3in]{virtuoso-figures/malloc.pdf}
    \caption[A malloc replacement that summarizes the effect of
    \texttt{RtlAllocateHeap} in Windows using QEMU micro-ops]{A malloc replacement that summarizes the effect of
      \texttt{RtlAllocateHeap} in Windows using QEMU micro-ops. The
      summary reads the size of the allocation from the stack, has a
      placeholder op for the actual allocation, reads the return
      address, cleans arguments from the stack, and executes the
      return.}
	\label{mallocsum}
\end{figure}

\subsection{Dynamic Slicing and Trace Merging}
\label{sec:impl:subsec:dynslice}

\begin{algorithm}[t!]
    \caption[Trace Merging Algorithm]{Trace Merging Algorithm.  $P$ is the final, merged program,
  which consists of a number of $block$s and corresponding successors.
  $T$ is the set of traces. Each trace $t \in T$ consists of
  a sequence of $op$s. $slice[t]$ is the subset of $op$s in $t$ that
  are needed, initialized with a dynamic data slice on the output.
  $dyn\_slice()$ is an instantiation of the dynamic slicing algorithm, and
  $find\_block()$ identifies a basic block to which an $op$ belongs.}
\begin{algorithmic}[1]
 \STATE $slices \leftarrow  \emptyset, blocks \leftarrow \emptyset$
 \FOR {$t \in T$}
 	\STATE $slice[t] \leftarrow dyn\_slice(t, t.output)$
	\STATE $blocks[t] \leftarrow split\_basic\_blocks(t)$
\ENDFOR
\REPEAT
  	\STATE $P.blocks \leftarrow \emptyset, P.succs \leftarrow \emptyset$
	\FOR {$t \in T$}
		\FOR {$op \in slice[t]$}
			\STATE $op\_block \leftarrow find\_block(blocks[t], op)$ 
			\IF {$(op\_block \in P.blocks)$}        
				\STATE $p\_op\_block \leftarrow find\_block(P.blocks, op)$
				\STATE $p\_op\_block.add(op)$
			\ELSE
				\STATE $P.succs[op\_block] \leftarrow op\_block.succs$
				\STATE $P.blocks \leftarrow P.blocks \cup new\_block(op)$
			\ENDIF
		\ENDFOR
	\ENDFOR
	
	\FOR {$block \in P.blocks$}
		\IF {$P.succs[block].size > 1$}
			\STATE $conds \leftarrow find\_exit\_conds(block)$
			\FOR {$t \in T, block \in t$}
				\STATE $br\_ops \leftarrow dyn\_slice(t, uses(conds))$
				\STATE $slice[t] \leftarrow slice[t] \cup br\_ops$
			\ENDFOR
		\ENDIF
	\ENDFOR
	
	\FOR {$t \in T$}
		\FOR {$op \in slice[t]$}
			\FOR {$t\prime \in T,  t\prime \ne t, op \in t\prime$}
				\STATE \small $slice[t\prime] \leftarrow slice[t\prime] \cup op \cup dyn\_slice(t\prime, uses(op))$
			\ENDFOR
		\ENDFOR
	\ENDFOR
 
\UNTIL {nothing changed}
\end{algorithmic}
\label{alg:progmerge}
\end{algorithm}

After preprocessing the trace, our Trace Analyzer uses \emph{executable
dynamic slicing} \cite{Korel:1988} to trace the flow of information
through the program, starting with the output buffer specified in the
log. For a detailed discussion of the algorithm, refer to Korel and
Laski \cite{Korel:1988}; however, we will summarize the idea here. Note
that because we do not have access to the full program, our dynamic
slicing algorithm cannot calculate full control dependency information
as in the original dynamic slicing algorithm. Instead, we include
\emph{every} control flow statement (and its dependencies) that was
observed to have more than one successor in the dynamic control flow
graph. This is safe (in the program analysis sense of the word), but may
over-approximate the dynamic slice.

Dynamic slicing works backward through the trace, looking for
instructions that define the desired output data. For each instruction,
the set of data \emph{defined} by the instruction is examined. If the
data it defines overlaps with the working set, then the instruction
handles tracked data, so we must add it to the slice. The working set is
then updated by removing the data defined by the instruction and adding
the data \emph{used} by the instruction. The algorithm terminates when
top of the trace is reached. The output, an \emph{executable dynamic
slice}, is precisely the sequence of instructions used to compute the
data in the output buffer.

Virtuoso builds the final introspection program from a training set of
more than one trace. This is crucial to achieving reliability: except
in the case of fairly trivial introspections, a single trace is unlikely
to include enough code to adequately cover all important functionality.
Consider the fact that many of the kinds of introspections we want to
support, such as enumerating running processes or loaded modules,
involve traversing collections such as linked lists or arrays.
Typically, the code that accesses these data structures includes logic
(and therefore multiple paths through the program) to cover corner
cases, such as when the collection is empty.  If we want our final
introspection tool to be able to handle these uncommon (but hardly rare)
situations correctly, we must build our program from multiple traces. In
practice, to ensure reliability, we generate increasingly reliable
programs iteratively: after generating an initial program, it is tested
on a variety of system states, and the failing test cases are then used
as new training examples. Although this cycle of testing and training is
currently done manually, it could easily be automated.

The trace merging algorithm works as follows. First, a merged
control-flow graph is constructed from the control flow graphs of the
individual traces (lines 8--19 in Algorithm~\ref{alg:progmerge}).
Second (lines 20--28), for each block which has more than one successor
(meaning there was a branch or a dynamic jump), a slice is performed on
the data neccessary to compute the branch condition (i.e., x86 status
and condition flags or the dynamically computed jump target); this step
ensures that both loops and branches are faithfully reproduced in the
generated program. Third, Virtuoso performs a \emph{slice closure}
(lines 29--35): if an op is in some but not all of the slices, then it
is added to the other slices, and a new dynamic slice is performed on
the dependencies of that op (this has the same effect, and is done for
the same reason, as Korel and Laski's Identity Relation
(IR)~\cite{Korel:1988}). Finally, because each of these steps may have
introduced new ops into the slices in ways that require recomputing
information in previous steps, we repeat the process until a fixed point
is reached.

\subsubsection{Correctness}

We argue the correctness of our trace merging algorithm in two parts. First, the algorithm certainly reaches a fixed point and terminates within a finite number of iterations. The traces used as input are finite in number and length.  This means that the number of micro-operations initially marked as ``in a slice'' by the dynamic slicing algorithm of Korel (which has, itself, been shown to complete in a finite number of steps) is also finite.  Thus, the number of micro-operations as-yet unmarked is also finite.  With each iteration of the algorithm, we either add no new ops to a slice, in which case we have reached a fixed point and terminate, or we add some finite number of ops to some slices and loop.  Thus, the algorithm will, at worst, add one op to each slice with each iteration, ending with every op in every trace marked, but even this will happen in a finite number of iterations of the algorithm.

Second, to see that our algorithm correctly merges multiple traces, consider combining traces $T$ and $T'$, each of which (when we run our algorithm on it individually) produces programs $P_T$ and $P_{T'}$,  which each have the correct output given their respective input (we can assume this because in this base case our algorithm is equivalent to that of Korel and Laski \cite{Korel:1988}). By examining Algorithm~\ref{alg:progmerge}, when we produce a program $P$ from the combination of $T$ and $T'$, we can see that there are only two cases where $P$ will differ from $P_T$ or $P_{T'}$:
    
\paragraph{Case 1} We include a branch statement that was not included in $T$ or $T'$ by itself. But by including this branch, we include the ops necessary to decide which way to go at that branch. So when $P$ is run with the inputs provided to $T$, the branch will evaluate as it did in $T$, and the successor will be the same as for $P_T$. The same argument applies when $P$ is run with the inputs for $T'$.
    
\paragraph{Case 2} We include an op (and the ops necessary to compute the dependencies of that op) that was in $T$ and not $T'$ (or vice versa). Assume without loss of generality that the op was in $T$ and not $T'$. Then the ops added to $T'$ do not affect the output value along the path taken by $T'$ (or they would have been added by the dynamic slice that initialized $slice(T')$). So when $P$ is run with the inputs for $T'$, it will produce the same output as $P_{T'}$.

This argument extends by induction to show that given an arbitrary number of traces $\{T_1, \ldots, T_n\}$, if the corresponding programs $\{P_1, \ldots, P_n\}$ are correct with respect to their inputs, the merged program will be correct with respect to all their inputs. We have also validated the correctness of our algorithm experimentally, as we describe in Section \ref{sec:results:subsec:reliability}.

\subsubsection{Fortuitous Coverage Enhancement}

We have demonstrated that a merged program $P$ can surely execute
correctly on guest states from one of its constituent traces. However,
aside from this additive effect, combining traces can also allow the
program to generalize better to guest states not seen during training.
To see why this is the case, consider Figure~\ref{mergesynergy}, which
depicts two program paths that are merged by our algorithm. In addition
to the two paths covered by each individual trace ($A \rightarrow C
\rightarrow D \rightarrow E \rightarrow G$ and $A \rightarrow B
\rightarrow D \rightarrow F \rightarrow G$), the merged program also
covers paths $A \rightarrow C \rightarrow D \rightarrow F \rightarrow G$
and $A \rightarrow B \rightarrow D \rightarrow E \rightarrow G$).
Depending on the relationship between the branch conditions at $A$ and
$D$, these paths may be infeasible, but in the best case, the merged
program will be able to generalize to guest states that differ from the
training states along these paths.

In our experience with Virtuoso, we have noticed several instances of
this ``fortuitous'' coverage improvement. For example, while testing the
\textbf{pslist} program for Windows (see Section \ref{sec:results}), we
generated two versions of the program from two different training runs.
Out of three system states in our testing corpus, we observed the following
behavior:
\begin{itemize}
\item The first program worked correctly on states 1 and 2, but not
state 3.
\item The second program worked correctly only on state 1.
\item The merged program, generated from both training runs, worked
correctly on all three system states.
\end{itemize}
While this is an isolated example, it suggests that such non-additive
coverage improvements can boost the reliability of the generated
introspection programs. We hope to make this notion more rigorous in
future work, and explore its potential for producing more reliable
introspection programs in conjunction with static analysis.

\begin{figure}[t!]
\centering
\includegraphics[width=3.2in]{virtuoso-figures/MergeSynergy.pdf}
\caption[An example of a non-additive coverage improvement from combining two traces]{An example of a non-additive coverage improvement from combining two traces. Rather than the expected two paths covered, we in fact cover four program paths.}
\label{mergesynergy}
\end{figure}

\subsection{Translation and Runtime Environment}
\label{sec:impl:subsec:trans}

Our Instruction Translator (shown below the Trace Analyzer in Figure
\ref{running_example}) takes the sliced micro-op traces and creates
an equivalent Python program that can be run outside the VM. Currently,
the code produced takes the form of a plugin for Volatility
\cite{volatility}, a framework for volatile memory analysis written in
Python. Volatility was chosen because it already provides certain
facilities needed in our runtime environment, such as x86 virtual
address translation, and the ability to analyze the memory of a virtual
machine running under Xen \cite{barham:2003} (through the third-party
extension PyXa, included in the XenAccess library \cite{Payne:2007nu}).
Moreover, it features an API that makes it easy to layer new
functionality on top of existing address space objects, which we use to
implement copy-on-write semantics for the guest memory (see below).

The translation of QEMU micro-instructions to host code is fairly
direct. The translation works one basic block at a time, starting with
the output of Algorithm~\ref{alg:progmerge}. Each individual op is
mapped to a simple Python statement. At the end of the block, if there
is only one successor, an unconditional jump to that successor is
produced. Otherwise, the appropriate conditional or dynamic jump is
output. 

The runtime environment itself, shown on the right in Figure
\ref{running_example} is implemented as a plugin for Volatility. The
introspection tool is output by the trace analyzer as a dictionary of
blocks of Python code, keyed by the EIP of the original x86 code.  The
plugin performs some initial setup, and then loads and executes the
block marked ``START''. When the block finishes, it sets a variable
named \texttt{label} that determines the successor (this variable can be
set conditionally, unconditionally, or dynamically, to implement the
three successor cases outlined above). This process continues until the
successor returned is ``EXIT''. At the end, the contents of the output
buffer are displayed to the user. The details of the runtime
environment, including input and output handling, are presented below.

Registers are modeled as variables in the generated code; thus, to
implement copy-on-write behavior for registers, we simply initialize the
values of the register variables before the generated code. Any
references to a register that occur before the generated code assigns to
it will therefore retrieve the value from the guest VM, as desired.
Assignments to registers, by contrast, will simply overwrite the Python
variable, and will not affect the running VM.

Access to memory is handled by translating load and store operations
into {\tt read} and {\tt write} operations on a copy-on-write address
space object in Volatility. The COW space is initialized by passing it
the currently active virtual address space for the VM (on x86 guests,
this boils down to using the current value of the guest's {\tt CR3}
register to map virtual addresses to physical). All reads and writes are
then done on this space, which implements copy-on-write as described in
Section \ref{sec:design:subsec:runtime}.

The runtime environment also provides a host-side memory allocator for
use with the malloc summaries described in
Section~\ref{sec:impl:subsec:preprocessing}. Before the program is run,
the runtime environment scans the guest page tables for an unused
virtual address range. It then adds page table entries (as always,
making changes to the copy-on-write space---the guest's state is not
modified) to create a heap that can be addressed as though it were
actually inside the guest. To prevent conflicts, the physical pages are
reserved by choosing a range above the amount of physical memory
available to the guest.\footnote{Although this means that in a 32-bit
environment we cannot support guests with 4 gigabytes of RAM, we note
that this is only a peculiarity of our implementation; see Section
\ref{sec:future} for details of how we plan to remove this limitation.
Also, the move to 64-bit architectures will provide ample physical
address space for Virtuoso to use if needed.} Reads and writes to that
physical range are redirected to the host memory area.

Inputs to the program are represented by an array named {\tt inputs}.
When the Instruction Translator comes across a micro-op that is marked
as reading input data, it translates it into a simple assignment that
takes the value from the input array. For example, if a micro-op such as
{\tt LDL T0, A0} is marked as needing the first input parameter to the
program, the code produced will be {\tt T0 = inputs[0]}. The {\tt
inputs} array is populated at runtime via a command line option to the
Volatility plugin.

Finally, output-producing operations are translated into two separate
Python statements. The first performs the write to memory normally,
while the second writes the data into a special output memory space and
tags it with a label that identifies which output buffer it was
associated with. Once the plugin has finished executing the translated
code, it dumps the contents of the output memory space. The user can
also optionally pass in a function to interpret the output (e.g., by
converting it from little-endian Unicode to a string, or to interpret a
64-bit integer as a human-readable time).
