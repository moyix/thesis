\section{Methodology}
\label{exsetup}

Signature search is essentially a classification problem: given an unknown piece of data, we wish to classify it as an instance of our data type or as something else. Our experiments, therefore, attempt to measure the performance of the signatures using standard classification metrics: false positives and negatives. A false positive in this case is a piece of data that matches the signature but would not be considered a valid instance~of the data structure by the operating system. Conversely, a false negative is a valid instance that is not classified as such by our signature. False negatives represent cases where  the attacker could successfully evade the signature, whereas false positives could introduce noise and make it difficult for to tell what processes are actually running. 

For our purposes, we only consider false positives that are syntactically invalid. We note that an attacker could generate any number of false positives by simply making copies of existing kernel data structures. These structures would be semantically invalid (the operating system would have no knowledge of their existence), but would be detected by a signature scanner. The possibility of such ``dummy'' structures is a known weakness of signature-based methods for finding kernel data structures \cite{Walters:2007to}; however, a solution to this problem is outside the scope of this work.

For our experiments, we chose to generate a signature for the Windows \texttt{EPROCESS} data structure, which holds information related to each running process. This structure was chosen because it is the most commonly hidden by malicious software, and there are a number of existing signature-based tools that attempt to locate this data structure in memory \cite{volatility,Betz:nx,Schuster:2006eb}. We compare the success of our signature with these tools. However, our work can also be applied to generate signatures for other data structures.

\subsection{Profile Generation and Fuzzing}
\label{meth:profile}
\label{meth:fuzzing}

During the profiling stage, we examined access patterns for fields in the \texttt{EPROCESS} data structure. To ensure that our data represented a wide range of possible application-level behavior, we chose twenty different programs that performed a variety of tasks (see Table \ref{app_profile_table} for a full list). To obtain a profile, we first launched the application and noted the address of its associated \texttt{EPROCESS} structure using the kernel debugger, WinDbg. We then instructed the Xen hypervisor to monitor access to the page, and used the application for a minimum of five minutes.

\input{dssig-apps_table}

We note that in addition to differences caused by the unique function performed by each application, other activities occurring on the system may cause different parts of the data structure to be exercised. In an attempt to isolate the effects caused by differences in program behavior, as each profile was generated we also used the system to launch several new tasks (Notepad and the command shell, \texttt{cmd.exe}), switch between interactive programs, and move and minimize the window of the application being profiled. 

After profiling the applications, we picked only the features that were accessed in all twenty applications. This choice is conservative: if there are applications which do not cause a particular field to be exercised, then it may be possible for an attacker to design a program that never causes the OS to access that field. The attacker would then be able to modify the field's value at will and evade any signature that used constraints on its value.

As described in Section \ref{arch:fuzzing}, features that were accessed by all programs profiled were fuzzed to ensure that they were difficult to modify. Checking that the \texttt{EPROCESS} data structure is still functioning after each fuzz test is much simpler if the associated program has known, well defined behavior. For this reason, we chose to create a program called \texttt{network\_listener} that opens a network socket on TCP port 31337, waits for a connection, creates a file on the hard drive, and finally exits successfully. The baseline snapshot was taken just after launching \texttt{network\_listener} inside the guest VM.

Because the program behavior is known in advance, the test to see if the OS and program are still working correctly ($\phi$) becomes simple. From the host, we perform the following tests on the virtual machine:

\begin{enumerate}
\item Determine if the virtual machine responds to pings.
\item Check that the program is still accepting connections on port 31337.
\item Check for the existence of the file written by the application (using the VMware Tools API).
\end{enumerate}

If all tests pass, then $\phi$ returns true, indicating that the modification was accomplished without harming OS or program functionality. If instead $\phi$ returns false, then the OS has crashed or some aspect of the program associated with our \texttt{EPROCESS} instance has stopped functioning. This latter case indicates that the OS will not accept arbitrary values for the field, and provides evidence that we can safely build a signature based on the field.

\subsection {Signature Generation and Evaluation}
\label{meth:siggen}

Finally, we generated a signature using the method described in Section \ref{arch:siggen}. The features chosen were the 15 most robust, as measured by the tests done during the fuzzing stage. For each of these fields, we extracted from our corpus of memory images (our training set) a list of the values it contained for all processes found in the image. The four images in the training set were not infected by malware, and were taken from systems running the 32-bit version of Windows XP, Service Pack 2. Processes were located in the memory image by walking the operating system's process list; in the absence of maliciously hidden processes, this serves as ``ground truth'' for the list of valid process data structures. We then used our signature generator to find constraints on the observed values. The signature generator outputs a plugin for Volatility that can be used to search for a data structure matching the constraints found in a memory image.

The generated scan plugin was used to search for processes in a number of memory images. For this purpose, we used two images provided by the NIST Computer Forensic Reference Data Sets (CFReDS) project \cite{CFReDS} and a paused virtual machine on which a process had been hidden by our own custom rootkit. The number of false positives and negatives were measured for each test image, and compared against two existing signature-based tools, PTFinder \cite{Schuster:2006eb} and Volatility's \texttt{psscan2} module.\footnote{Note that Volatility also includes a process scanner called \texttt{psscan}. This scanner uses the same constraints as PTFinder, and hence is vulnerable to the same evasions, so we do not consider it here.}

Our custom malware, which is a slightly modified version of the FU Rootkit \cite{FuRootkit}, hides processes using DKOM (as in the original FU), and additionally attempts to evade known process signatures by zeroing non-essential fields in the process data structure. The fields modified, shown in Table \ref{fu_fields}, were chosen by finding those fields that were used by common scanners but that our initial structure profiling indicated were unused by the OS.

\begin{table}[t!]
\centering
\caption{Fields zeroed by our modified FU Rootkit, along with the scanners that depend on that field.}
\label{fu_fields}
\begin{tabular}{ l c }
\textbf{Field} & \textbf{Used by} \\
\hline
ThreadListHead.Blink & Volatility (psscan2)\\
Pcb.Header.Type & PTFinder \\
Pcb.Header.Size & PTFinder \\
WorkingSetLock.Header.Type & PTFinder \\
WorkingSetLock.Header.Size & PTFinder \\
AddressCreationLock.Header.Type & PTFinder \\
AddressCreationLock.Header.Size & PTFinder \\
\end{tabular}
\end{table}
