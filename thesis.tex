\documentclass{gatech-thesis}
% The default options:
%   \documentclass[11pt,letterpaper,oneside,%
%       doublespaced,normalmargins,final]{gatech-thesis}
% will generate a document that conforms to the graduate studies office
% guidelines.

 \ifx\pdfoutput\undefined
   \usepackage[dvips,final]{graphicx}  % using latex+dvips
   \usepackage[dvips,usenames]{color}
 \else
   \usepackage[pdftex,final]{graphicx} % using pdflatex
   \usepackage[pdftex,usenames]{color}
 \fi

\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{boxedminipage}
\usepackage{cite}
\usepackage{float}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{multirow}
\usepackage{subcaption}
\usepackage{url}

%define stuff in preamble
 \degree{Doctor of Philosophy}
 \department{School of Computer Science}
 \title{Understanding and Protecting Closed-Source Systems through
 Dynamic Analysis}
 \author{Brendan Dolan-Gavitt}
 \principaladvisor{Wenke Lee}
 \firstreader{Patrick Traynor}
 \secondreader{Mustaque Ahamad}
 \thirdreader{Jonathon Giffin}[HP Fortify][]
 \fourthreader{Weidong Cui}[Microsoft Research][]
 \submitdate{December 2014}
 \approveddate{21 August 2014}
 \copyrightyear{2014} %add one if thesis submitted in Dec.
 \copyrighttrue            % default

% \thesisproposalfalse       % default
% \titlepagetrue             % default
% \signaturepagetrue         % default
% \figurespagetrue           % default
% \tablespagetrue            % default
% \contentspagetrue          % default
% \dedicationheadingfalse    % default
% \bibpagetrue               % default
% \strictmarginstrue         % default

\bibfiles{thesis-bib}

\begin{document}
\bibliographystyle{gatech-thesis}
\setchaptertocdepth{2}
%
\begin{preliminary}

\begin{dedication}
    \begin{center}
    To my parents, \\
    Terry Dolan and Phil Gavitt.
    \end{center}
\end{dedication}

\begin{acknowledgements}

I am deeply grateful to my current advisor, Wenke Lee, for his guidance,
support, and understanding as I made my way, however circuitously,
through the PhD program. My erstwhile advisor Jon Giffin was an
unfailing and invaluable source of insight and stimulating conversation;
I will always fondly remember hours-long conversations in my first few
years at Georgia Tech on topics ranging from program analysis and
algorithms to Buffy the Vampire Slayer. Finally, Patrick Traynor's
Destructive Research course my first fall semester helped kickstart my
research career, and I have always found his advice and insight
valuable. My mentors outside of Georgia Tech have also been crucial to
my development: Weidong Cui and David Molnar of Microsoft Research were
sources of inspiration and invaluable guidance during my summer in
Seattle. I would especially like to thank Tim Leek of MIT Lincoln
Laboratory; during the five years I have worked with him he has proved
himself to be not only a brilliant collaborator and wise mentor but also
a true friend.

My friends in Atlanta, Boston, and elsewhere have made the highs higher
and the lows endurable, and I could not ask for a smarter or kinder
group of people in my life. My best friends deserve special mention:
Emily Wang, my oldest and dearest friend, whose friendship has nourished
me through hard and lean times; and Ying Xiao, the most intelligent and
learned man I know, who always challenges me to do and be better than I
think I can. I also give thanks, in no particular order, to Sarah
Montgomery, Randall Munroe, Rosemary Mosco, Patrick Hulin, Anthony Eden,
Josh Hodosh, Ryan Whelan, Catherine Grevet, Yacin Nadji, Chris Berlind,
Emma Cohen, Sara Krehbiel, Anita Zakrzewska, Prateek Bhakta, and
everyone else that deserves a place here but whom I have momentarily
forgotten. Your love and friendship has made all the difference these
past six years.

Annuska Riedlmayer has, for the past four amazing years, been my love,
my partner in adventure, my patient confidant, and my constant
companion. Thank you for your patience as I worked on this thesis so far
away, for teaching me so much, and for bringing so much joy into my
life.

Thomas Dolan-Gavitt, my brother and friend, talented writer and
filmmaker, and all-around wonderful human being, thank you for so many
late-night talks, shared ideas, movie and music recommendations, and
moral support. I love you, bro.

Finally, I must thank my parents, Terry Dolan and Phil Gavitt, to whom
this thesis is dedicated, for their unconditional love and support.
Growing up, I never once doubted that they placed the interests of their
children first, and I know they made many sacrifices to ensure that we
had the best education possible; I hope that my work, in some small
measure, can stand as a testament to their efforts.

\end{acknowledgements}
%
\contents
%
\begin{summary}

The modern world is critically dependent on complex software whose
internal details are often tenuously understood, even by their creators.
In order to properly protect deployed systems whose authors may be
unable or unwilling to divulge critical details of their systems'
operation, we must develop ways of understanding deployed binary
software; furthermore, because the amount of deployed code vastly
outstrips our ability to analyze it manually, we need techniques that
can automate these analyses. The techniques of program analysis,
originally created to help test software in development, have
increasingly been used by those in the security community to help
understand binary programs without cooperation from code creators. These
analyses have typically focused on static analysis of the structure of
the code in question; while this can bear significant fruit at small
scales, it usually cannot handle large-scale software such as operating
system kernels.

In this dissertation, we focus instead on dynamic analyses that examine
the data handled by programs and operating systems in order to divine
the undocumented constraints and implementation details that determine
their behavior in the field. In doing so, we make practical a wide
variety of new security applications that previously depended on manual
reverse engineering, including RAM forensics and virtual machine
introspection.

First, we introduce a novel technique for uncovering the constraints
actually used in OS kernels to decide whether a given instance of a
kernel data structure is valid. Such information is critical in
developing intrusion detection systems and forensic tools that look for
hidden and unlinked structures, but previous approaches to finding these
constraints relied on unprincipled guesswork. Our system dynamically
probes for enforced invariants by mutating the content of existing data
structure instances and monitoring the operating system's response to
determine whether the modification has degraded system functionality.
In doing so we can infer which constraints on the structure's fields are
actually enforced.

Next, we further enable progress in secure system monitoring and
forensics by tackling the semantic gap problem -- the disparity in
semantic understanding between what is needed by security systems and
what is provided by low-level views (e.g., raw physical memory) of
running systems. We present a pair of systems that allow, on the one
hand, automatic extraction of whole-system algorithms for collecting
information about a running system, and, on the other, the rapid
identification of "hook points" within a system or program where
security tools can interpose to be notified of events such as file or
URL access, cryptographic key generation, and kernel log messages.
Together, these bridge the semantic gap by allowing periodic passive
monitoring of system state and immediate notification of specific
security events, respectively. Each relies on dynamic analyses that
start with examples of security-relevant data and identify code within
the system that handle or report such data.

Finally, we present and evaluate a new dynamic measure of code
similarity that examines the content of the data handled by the code,
rather than the syntactic structure of the code itself. This problem has
implications both for understanding the capabilities of novel malware as
well as understanding large binary code bases such as operating system
kernels.

\end{summary}
\end{preliminary}
%
\input{intro}
\input{chapter1}
\input{chapter2}
\input{chapter3}
\input{chapter4}
\input{conclusion}
%
\appendix

\input{tzb-appendix}

%
\begin{postliminary}
\references
\end{postliminary}
\end{document}
