\section{Virtuoso Design}
\label{sec:design}

\begin{figure}[t!]
\centering
\includegraphics[width=3.2in]{virtuoso-figures/overview.pdf}
\caption[A high-level conceptual view of our system for generating
introspection tools]{A high-level conceptual view of our system for generating
  introspection tools. This view corresponds to the training and
  analysis phases shown in Figure \ref{running_example}. The programmer
  creates an in-guest program that computes the desired introspection
  quantity by calling standard OS APIs. This in-guest program is then
  run repeatedly under various system states, and the instructions
  executed are logged. These instruction traces are then analyzed to
  isolate just the instructions that compute the introspection quantity,
  merged into a unified program, and then translated into an
  out-of-guest introspection tool.}
\label{system_overview}
\end{figure}

\begin{figure}[htbp]
\centering
\fbox{
\includegraphics[width=3in]{virtuoso-figures/getpsname.pdf}
}
\caption[A sample program that gets the name of a process]{A complete sample program that gets the name of a process given
  its PID under Windows. Programs such as these are easy to write from
  inside the operating system using documented APIs.
  \texttt{GetProcessImageFileName}, \texttt{OpenProcess}, and
  \texttt{malloc} are standard API calls known to Windows programmers.
  \texttt{vm\_mark\_buf\_in} and \texttt{out} notify Virtuoso of input
  and output.}
\label{introprog}
\end{figure}

At a high level, Virtuoso creates introspection tools for an operating
system by converting in-guest programs that query public APIs into
out-of-guest programs that reproduce the behavior of the in-guest
utilities. This is done in three phases (Figure \ref{system_overview}):
a \emph{training phase}, in which an in-guest program that computes the
desired information is run a number of times, and a record of each
execution is saved; an \emph{analysis phase}, which extracts just the
instructions involved in computing the data required for the
introspection, merges the traces into a single program, and translates
that program into one that can be executed from outside the virtual
machine; and finally a \emph{runtime phase}, in which the generated
program is used to introspect on a running virtual machine. These steps
are described in more detail below.

\subsection{Training}
\label{sec:design:subsec:introprog}

The training phase is shown in Figure \ref{running_example}, our running
example: the training program, \texttt{getpsfile.exe}, is run several
times in the training environment to capture a variety of program paths,
producing a number of instruction traces. For this stage, we assume the
developer can create such small, in-guest programs that compute the data
needed for the introspection tool. This places a very minimal burden on
the programmer: because ordinary programs running inside the target
operating system need to query many of the same kinds of information
desired by introspection tools, OS designers typically make rich APIs
available for finding out this information. Additionally, such APIs are
generally easy to use, stable, and well-documented, as they are the
primary interface through which programs talk to the OS.

A sample program that retrieves the name of a process under Windows
is shown in Figure \ref{introprog}. The program, annotated with markers
that indicate where to begin and end tracing (\texttt{vm\_mark\_buf\_in}
and \texttt{out}), invokes the Windows API functions \texttt{OpenProcess}
and \texttt{GetProcessImageFileName} to get the process name. The
annotations also record the addresses of input and output buffers for
the program; these will ground the data flow analysis performed to help
identify what portions of the system's execution are necessary to
compute the introspection quantity (in this case, the
process name).

Even for simple programs like the one described above, there may be a
number of different execution paths taken through the program depending
on the state of the OS when the program is executed. To produce a
program that works correctly in all situations, we run the program
repeatedly in an instrumented environment and collect traces that record
all instructions executed by the program and operating system. These
execution traces are then analyzed and merged together to produce an
out-of-guest introspection tool that can be used to reliably provide
security-relevant information for monitoring applications.


\subsection{Analysis}
\label{sec:design:subsec:ta}

Although the execution traces gathered in the training phase contain all
the instructions needed to compute the introspection quantity, they also
contain a large amount of extraneous computation. This extra ``noise''
is present because our traces record the execution of the whole system:
in addition to computing the introspection quantity, the traces also
contain unrelated events such as interrupt handling and unrelated
housekeeping tasks performed by the kernel. Because we are only
interested in the code that is necessary for producing a working
introspection program, we must excise these extraneous parts of the
traces. We accomplish this by first throwing away or replacing parts of
each trace that we know \emph{a priori} to be unrelated to the
introspection, such as hardware interrupts and memory management. Next,
we identify the instructions that actually compute the output by
performing a dynamic data slice \cite{Korel:1988} on each trace.
Finally, we merge the slice results across basic blocks and traces,
producing a unified program that can be translated into an out-of-guest
introspection routine.

In order to produce a working routine, the inputs and outputs to the
training program must be identified. To do so, we look for places in the
trace where buffers marked as inputs are used. These instructions are
marked as places where input data is used so that, at translation time,
we can generate code that splices in the program inputs at these points.
Similarly, as the location of the output buffer may change depending on
the current environment (for example, if the output buffer is on the
stack, its location will change depending on the value of {\tt ESP} when
the introspection tool is run), the analysis determines which
instructions are immediately responsible for writing the data to the
output buffer. These ``output-producing instructions'' are marked; the
translator will handle such instructions specially, so that the output
data can be found independent of the runtime CPU state.

Once the instruction trace has undergone slicing, merging, input
splicing and output instruction marking, the merged traces are
translated into an out-of-guest introspection program. The
programs generated by Virtuoso consist of a set of basic blocks together
with successor information. Within a basic block, each individual
instruction is implemented by a small snippet of code in a high-level
language. The use of a high-level language allows our generated programs
to be usable in many different contexts: forensic memory analysis,
virtual machine introspection, and low-artifact malware analysis.

In our example (Figure \ref{running_example}), we can see that the Trace
Analyzer and Instruction Translator together comprise the analysis
phase, and create the final introspection program \texttt{getpsfile}
from the instruction traces generated during training.

\subsection{Runtime Environment}
\label{sec:design:subsec:runtime}

The translated code, however, cannot be executed directly. Instead, it
must be provided with an appropriate \emph{runtime environment} in which
to execute. The runtime environment (shown on the right in
Figure~\ref{running_example}), is installed on the host and runs the
translated instructions in a context that gives them access to the
resources (such as CPU registers and memory) of the guest virtual
machine without perturbing its state.

In particular, low-level operations that affect registers or memory are
wrapped so that they enforce \emph{copy-on-write (COW)} behavior.
Whenever a write to memory occurs, it is redirected to the COW memory
space (shown in the right panel of Figure \ref{running_example}); when a
read occurs, it first checks whether the data exists in COW memory. If
so, it reads from the COW space. Otherwise, the read is serviced
directly from guest memory. A similar scheme is used to handle register
access. This allows introspections to access the state of the running
system without interfering with its operation.

There is some subtlety to the question of how to obtain values from the
memory of the guest. Certain data seen during training may be specific
to the training program (for example, static data such as strings),
while other data may be part of the system's state as a whole. In the
former case, we would want to use the values seen in training (as they
may not be available in the environment in which the introspection
program is run), whereas in the latter we would want to obtain the
values from the guest itself (since the point of introspection, after
all, is to obtain the state of the system). To differentiate between
these two types of data, we currently make the simplifying assumption
that \emph{kernel} data seen during training is global, and should be
read from the guest, while \emph{userland} data is specific to the
training program, and should be saved during training and re-used at
runtime. This reflects the nature of the introspections we wish to
capture, which obtain information about the state of the system as a
whole rather than any individual process.

Although this policy currently limits the type of introspections that
can be generated to those that inspect systemwide state, one could
define other policies that make introspection into specific processes
possible. If Virtuoso were provided with some means of locating the
appropriate process context at runtime, one could define a policy that
reads all data directly from the guest, allowing introspection into the
memory of a specific process.

When performing an introspection (even with hand-generated tools), the
guest CPU is paused in order to prevent the contents of memory from
changing during the introspection. It might seem that one could keep the
CPU running, improving performance by allowing the introspection to be
carried out in parallel with the guest's execution. This performance
boost would come at the cost of reliability, however, as changes to the
underlying data examined by the introspection tool would almost
certainly cause it to crash or report incorrect results.  In addition,
we have noticed that most generated introspections must be run when the
CPU is in user mode. This is because the traces are recorded based on a
userland program, and so some assumptions may be made about the state of
global registers in userland that do not hold in kernel mode. To ensure
reliability, our runtime environment polls for a user-mode CPU state and
pauses the guest before performing any introspection.\footnote{Rather
than polling, it is also possible to set up a callback within the guest
whenever there is a transition from kernel to user mode, using the
techniques described by Dinaburg et al.~\cite{Dinaburg:2008}.}

Finally, the Instruction Translator has special handling for
instructions marked by the Trace Analyzer as inputs or outputs.
Instructions that require input are replaced with equivalent
instructions that explicitly take input from the environment in which
the introspection tool runs. Output-producing instructions are
translated so that their output is placed in a special buffer at a known
location; when the program terminates, the contents of this buffer
containing the desired introspection quantity are returned so they may
be used by security tools. In our running example (Figure
\ref{running_example}), we can see that the generated introspection
program is provided with the PID 384 as input, and produces the output
``chrome.exe''.

Note that the contents of the output may still need to be interpreted
(for example, by decoding the binary data as a string or integer, or
even more complex interpretations such as displaying a timestamp in
human-readable form). We provide a basic facility by which the user can
provide a function that interprets the output from the runtime
environment; however, we do not attempt to include this functionality in
the generated introspection tool itself. We feel that this approach is
justified, as the output will be the same as that produced by APIs
within the guest OS, which we have assumed are well-documented.
Therefore, the format of their outputs will most likely also be
documented by the OS vendor.
