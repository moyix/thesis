\chapter{Conclusion}
\label{chap:conclusion}

In this thesis we have presented a number of novel techniques that
uncover high-level semantic information about closed-source programs and
operating systems for security. The algorithms presented in
Chapters~\ref{chap:dssig}--\ref{chap:tzb} have significantly improved
the feasibility of virtual machine based intrusion detection and memory
forensics by reducing the ``semantic gap'' between high- and low-level
views of vulnerable operating systems. We also presented a new technique
that provides the first steps toward a general system for understanding
the behavior of unknown programs; moreover, we found that this technique
outperformed the state of the art on the simpler task of matching
functions between different compiler optimization levels.

We conclude by discussing several large, open problems in both program
understanding and intrusion detection that can operate without trusting
the OS.

\subsection{The ``Strong'' Semantic Gap}

In a recent paper surveying virtual machine introspection
techniques~\cite{Jain:2014}, Jain et al. noted that most work on the
semantic gap (including our own) is forced to assume that despite
malicious modification, the underlying semantics of the system's data
structures remain unchanged. This assumption has been challenged,
however, by attacks such as Direct Kernel Structure Manipulation
(DKSM)~\cite{Bahram:2010}, which alters the layout of kernel data
structures and thereby invalidates the semantic meaning learned by
systems such as those described in Chapters \ref{chap:dssig}
and~\ref{chap:virtuoso}. Jain et al. name this more difficult challenge,
of maintaining security monitoring in the face of semantic manipulation
attacks, the ``strong'' semantic gap problem, and conclude that there
has been very little research on it.

In the limit, it seems clear that without imposing constraints (either
in the threat model or, preferably, through controls enforced by
hardware) on modifications malicious code running at the same privilege
level as the operating system can make, maintaining security monitoring
is impossible. For example, kernel malware may simply set up its own
small operating system and scheduler and share time between it and the
main OS; while this may seem farfetched, some proof-of-concept
rootkits~\cite{90210:2005} do go so far as to replace the OS scheduler
with their own.

Can automated reverse engineering and program understanding be made to
work even in this highly adversarial scenario? That is, could we devise
analyses that are both fast and accurate enough to analyze a potentially
unknown operating system and extract actionable security information
about its behavior? Or, failing that, what is the minimum set of
restrictions we need to enforce in order to guarantee that our semantic
assumptions cannot be violated?

\subsection{Understanding Embedded Systems}

While understanding \emph{software} has been the focus of this thesis,
the proliferation of embedded computing devices poses new challenges. In
such systems, simply analyzing the software on the device may be
insufficient to evaluate its security and protect it from attack.
Instead, we may need to understand how its hardware peripherals
work---what the valid ranges of inputs and outputs are, what states it
can get into, what attack surface it exposes to the world, and what
effect its hardware can have on the physical world (e.g., can your
printer actually be forced to catch fire?).

Some of these questions are well beyond the scope of our techniques: it
is unlikely we will be able to apply software analysis to reverse
engineer the physical properties of hardware, for example. However, it
may be possible in some cases to infer certain properties of the
hardware through close analysis of the software interfaces used to
communicate with it. In some preliminary work not reported in this
thesis, we were able to infer constraints on values returned from
embedded hardware peripherals by performing symbolic
execution~\cite{King:1976} on its firmware and solving path constraints
that followed a successful (non-crashing) path through the firmware boot
process. It remains an open question, however, how much we can discover
about hardware in this way---complicated peripherals may have
complicated state machines or even be full-fledged computing devices in
their own right, which places fundamental limits on what fully automated
techniques can achieve. As with undecidable problems in program
analysis, however, a great deal of progress may be achievable with
careful application of heuristics and assumptions about common cases.

\subsection{Reverse Engineering-Friendly Construction}

The programs and systems that are the targets of our analyses in this
thesis were all designed to be, if not actively hostile (in the case of
malware), then at least indifferent to the needs of outsiders wishing to
understand their internal details. In addition to developing ever-more
powerful ways to tease out the hidden implementation details of such
systems, we might instead turn the question around and ask whether we
can construct programming languages and tools that expose the internals
of a program in a self-documenting way. For example, a compiler might be
able to automatically produce parsers for the in-memory data structures
used by a program, or generate a list of useful hook points throughout
the code, such as places where data crosses marked security boundaries.

In the realm of hardware this is likely to be even more difficult, both
because of the lack of standardized tools for hardware design and the
reticence of hardware manufactures to give out what they consider trade
secrets. Nevertheless we may seek methods that augment the tools used in
hardware design so that they produce machine-readable descriptions of
the device's functionality and the protocols needed to interact with it.
This would provide numerous benefits for security analyses: the
descriptions could inform static analyses of the firmware that interacts
with such hardware, and dynamic analyses that rely on being able to
emulate hardware platforms could potentially automatically generate
emulated peripheral models. As a side benefit, having such open hardware
descriptions readily available would open up such embedded systems to
alternative operating systems, improving user freedom.

\subsection*{Conclusion}

The solutions to these problems will determine the privacy and security
of the next generation of computing devices. It is vital that we have
ways of quickly understanding the attack surface and vulnerabilities of
embedded systems that can affect the physical world, be able to quickly
reverse engineer novel malware, and provide protection for legacy
systems until they can be replaced by newer and hopefully more secure
versions.

Even at the level of the common user, it is important that we have at
our disposal robust automated techniques for understanding the software
we use. Events such as the discovery of censorship and surveillance in
the Chinese version of Skype~\cite{Knockel2011} have shown that reverse
engineering can be a way of combating infringements on human rights, and
the proliferation of advertising as the primary way of monetizing mobile
applications and web sites means that we must be even more vigilant and
aware of what private information is exposed to software eager to
profile and target us.

If we seek to have control over the computing systems that have
increasingly become enmeshed in our lives, we must start by
understanding how they work and what they do, and automated program
analysis techniques are a vital part of achieving this.
