\section{Evaluation}
\label{sec:results}

In this section, we evaluate Virtuoso using a number of different criteria: generality, reliability, security, and performance. To evaluate the generality of Virtuoso, we look at the diversity of operating systems and the different types of programs for which introspections can be generated. Next we discuss the \emph{reliability} of the resulting introspection programs---that is, their ability to function correctly when examining a system at runtime. Finally, we examine the resilience to attack of programs output by Virtuoso, and we discuss their runtime performance.

\subsection{Generality}
\label{sec:results:subsec:generality}

Because Virtuoso uses very little domain knowledge about the target OS, it is easy to generate new introspection programs for operating systems that run on the x86 architecture. To demonstrate this capability, we created six training programs for three different OSes. These programs were chosen because they represent common actions that might be needed for passive and active security monitors: a security system may need to list drivers to audit the integrity of kernel code, or enumerate processes in order to scan them for malicious code, and so on. Some of these functions are also implemented for Windows targets in tools such as Volatility \cite{volatility}; however, our own tools required no reverse engineering and were generated automatically.

The six programs performed the same function on all three operating systems:

\begin{itemize}

\item \textbf{getpid} Gets the process ID of the currently running process.

\item \textbf{gettime} Retrieves the current system time.

\item \textbf{pslist} Computes a list of the PIDs of all currently running processes.

\item \textbf{lsmod} Retrieves a list of the base addresses of all loaded kernel modules.\footnote{In the Linux version, this reads the contents of \texttt{/proc/modules}, which contains both the names and base addresses of kernel modules, so \texttt{getdrvfile} is not needed on Linux.}

\item \textbf{getpsfile} Retrieves the name of the executable associated with a given PID.

\item \textbf{getdrvfile} Retrieves the name of a loaded kernel module, given its base address.

\end{itemize}

The three operating systems used were Windows XP SP2 (kernel version 5.1.2600.2180), Ubuntu Linux 8.10 (kernel version 2.6.27-11), and Haiku R1 Alpha 2 \cite{haiku}. Windows and Linux were chosen because they are both in common use,\footnote{Although use of OS X is also widespread, its design isolates the kernel in a separate address space. Due to limitations in our current implementation discussed in Section \ref{sec:future}, we were unable to test OS X introspection.} and thus represent practical, real-world scenarios. Haiku, a relatively obscure clone of BeOS, was chosen for two reasons: (1) to our knowledge, we are the first to do virtual machine introspection or memory analysis on a Haiku target, making it a compelling example of Virtuoso's ability to deal with diverse operating systems; and (2) none of the authors had any prior knowledge of the internals of the Haiku kernel, so there was no way to ``cheat'' by incorporating additional domain knowledge.

\begin{figure}[t!]
\begin{centering}
\input{virtuoso-general.tab}
\caption[Results of testing a total of 17 programs across three different operating systems]{Results of testing a total of 17 programs across three different operating systems. ``Original'' refers to the size of the trace before any processing, ``Post'' is the size after interrupt filtering and malloc replacement, and ``Final'' gives the size of the program after slicing; all sizes given are in number of IR ops. The numbers given refer to processing of a single dynamic trace.}
\label{general}
\end{centering}
\end{figure}

The results of our testing are shown in Figure \ref{general}. To verify that each program worked correctly, we ran it on a sample memory image taken at a different time from when the trace was captured, and then verified the output by hand (with help from existing in-guest tools). The numbers shown are for programs generated from a single trace.

With one exception, the generated programs all worked correctly---\texttt{gettime} for Haiku failed to obtain the correct system time. Upon inspection, we determined that this was due to the way Haiku keeps time: rather than continuously updating some global location, Haiku stores the boot time and then calculates the current time by calling \texttt{rdtsc} to read the Time Stamp Counter (TSC), which contains the number of cycles executed since boot. Because our runtime environment does not have access to the TSC when operating on a memory image, the time returned by \texttt{gettime} is incorrect. However, we manually verified that when provided with the correct TSC value, the generated program functions correctly.

Figure \ref{general} also shows the effect of preprocessing and slicing on the size of the generated program. One clear point emerges from examining these figures. Preprocessing removed between 17\% and 97\% of the initial traces (it removed 55\% on average), and slicing reduced the number of remaining instructions in the trace by 40\%--89\% (56\% on average). Because the resulting programs do, in fact, work, we can conclude that a large portion of the computation seen in the initial traces is irrelevant to the final output. Also, we can observe that were we to run all the code seen in the trace, the time required to run the introspection programs would dramatically increase.

\subsection{Reliability}
\label{sec:results:subsec:reliability}

Because our introspection programs are based on dynamic analysis of the training programs, they do not, in general, contain all the code needed to account for every eventuality the introspection program may encounter. This limitation is inherent to the use of dynamic methods and is a well-known problem in program testing. In this section, we describe the results of our experiments on the reliability of the generated introspection programs, techniques for improving their coverage, and the motivation behind our use of
dynamic analysis.

\begin{figure}[t!]
\begin{centering}
\includegraphics[width=3.5in]{virtuoso-figures/crossval_k_vs_ratio_err_mono.pdf}
\caption[Results of cross-validation for the \texttt{pslist} program on Windows]{Results of cross-validation for the \texttt{pslist} program on Windows. The error bars indicate the standard error of each sample.}
\label{crossval}
\end{centering}
\end{figure}

\paragraph{Testing}

To empirically test the reliability of programs generated using Virtuoso, we examined how the reliability of a single program (\texttt{pslist}, described in Section \ref{sec:results:subsec:generality}) was affected by training. We generated 24 traces of \texttt{pslist} running under Windows XP, taking a trace once every five minutes, starting just after the desktop appeared. During this time, we did the following actions:
\begin{itemize}
\item Opened a connection to a remote machine with PuTTY.
\item Browsed web pages with Google Chrome and Internet Explorer.
\item Played a game of Minesweeper.
\item Closed a non-responsive instance of Internet Explorer, and sent a crash report.
\item Played a game of Solitaire.
\end{itemize}
No special effort was made to induce a wide variety of system states such as low-memory conditions. Along with each trace, we also captured a snapshot of the memory and CPU state.

We then used cross-validation to estimate the reliability of the program as a whole: for each $k$ in the range $\{1 \ldots 12\}$  we took 50 random subsets of size $k$ from the set of all traces captured,\footnote{For $k=1$ there are only $24$ subsets, so our number of subsets in this case is $24$.} used these traces to generate an introspection program, and then tested the reliability of the program on the $(24-k)$ images corresponding to the remaining traces. We judged that the generated program had executed correctly if it produced the same output as the training program for that image. Figure \ref{crossval} shows the results of this testing. Each point represents the mean reliability of programs generated from $k$ traces.

We note that reliability appears to increase linearly with the number of traces used; since the reliability is bounded above by 1, it most likely approaches an asymptote as more traces are added and the number of unexplored paths in the program grows smaller. We also ran the generated programs on their training images, and, as expected, achieved a success rate of 100\%, validating the correctness of our trace merging algorithm.

In examining the data more closely, however, we observed that traces taken earlier in time (closer to system boot) were more reliable. To test this effect, we created an additional test data set of 24 images, again captured once every five minutes starting shortly after system boot. We then generated the \texttt{pslist} program from the first trace captured during our cross-validation test and no others. To our surprise, we found that this program had 100\% reliability on our new test data set.

By inspecting the source code to the Windows Research Kernel \cite{wrk}, we determined that the Windows operating system stores the image name of the process \emph{lazily}, waiting to generate the attribute until the first time some program requests it. So the first trace includes all the code needed to generate the process name, whereas later traces simply read the already-initialized data. When programs produced from these latter traces encounter a case where the process name is not yet initialized, they will lack the code necessary to do so. Thus, this caching effect may cause the cross-validation numbers given above to overestimate the difficulty of building reliable programs; in this case, the natural testing strategy of booting the system and taking several traces would have yielded a reliable program.

\paragraph{Improving Coverage}

In any case where testing finds that the generated program fails to function correctly, we can take a new trace based on the failing snapshot that should work correctly in that case. Although we will not be able to run the program precisely from the point where the failure occurred, in practice we have found that failing states tend to persist long enough that we can take a new trace that covers the failed snapshot. This allows us to iteratively improve coverage based on gaps found during training, until we reach a point where no further gaps are found. In future work, we hope to explore how techniques from the software testing community can help make this approach more rigorous. For example, we may be able to perform static analysis of the OS code to reveal missing branches, and then apply symbolic or concolic execution to determine what system state is necessary to go down that branch and improve coverage.

\paragraph{Static vs. Dynamic Analysis}

A natural question is why, given the coverage issues associated with dynamic analysis, our approach does not make use of static analysis. Although we considered static analysis, a number of difficulties make it an unattractive choice in this context. First, static analysis relies on the ability to reliably identify all the code associated with a piece of functionality. In the general case, accurate disassembly of x86 code is undecidable \cite{Linn:2003}, and even on non-malicious code, it can be quite difficult. Second, our slicing approach would be much less accurate if performed statically, particularly in the presence of dynamic jumps (which are common in kernel code, which makes extensive use of function pointer-based indirection) and dynamically allocated memory. Finally, static analysis requires much more domain knowledge about the target operating system: at minimum, the executable file format and the details of the loader must be known just to find the code to analyze.

To demonstrate the reasons why static analysis is inappropriate in this case, it is useful to look at a recent system, BCR (Binary Code Reutilization) \cite{Caballero:2010}, which also attempts to extract portions of a binary program for later re-execution, but does so using static analysis. First, we note that BCR does not use a purely static approach: it relies on dynamic analysis in its disassembly phase to resolve the targets of dynamic jumps. The authors do not clearly indicate how their system deals with any coverage issues that may arise from this use of dynamic analysis. Second, BCR employs significant amounts of domain knowledge to perform its code analysis. The executable file format, system API calls, and the mechanism by which imported functions are resolved must all be known in order to extract assembly functions. This reliance on domain knowledge both restricts the generality of BCR and limits the kinds of code it can extract---for example, BCR is unable to extract functions that make system calls directly. Virtuoso, by contrast, relies on no such domain knowledge and works with any code constructs supported by the x86 architecture.

Even if it were possible in our case to statically find every code path that could be taken by the program, it may not be desirable. For example, consider the case of a program written for Linux that retrieves sensitive information from the \texttt{/proc} filesystem. At some point in the execution of this program, the kernel will likely perform a check along the lines of \texttt{if (uid == 0)}. Although this check is a valid part of the overall program, it does not actually affect the \emph{value} computed by the program. If we use dynamic analysis and collect traces that succeeded, we will only ever see the branch where this condition evaluated true, and the check will not be incorporated into the resulting program. Although this is, strictly speaking, incorrect, it has the side effect of \emph{generalizing} the program: whereas the original code could only be executed in a context where the current user was root, the generated introspection utility will be able to compute the correct result from the context of any user. Additional research is required to determine if these ``undesirable'' branches can be automatically eliminated using a static approach.

\subsection{Security}
\label{sec:results:subsec:security}

The main motivation in the design of Virtuoso is to enable the rapid creation of secure introspection-based programs. Were security and unobtrusiveness of no concern, it would be sufficient to deploy an in-guest agent that queried existing OS APIs and simply trust that they had not been compromised. However, as security \emph{is} a concern, we must test that our system does, in fact, provide the hoped-for isolation from malicious changes to the guest operating system.

To demonstrate that the generated programs are immune to malicious changes in kernel code, we generated our Windows process lister (\texttt{pslist}, described in Section \ref{sec:results:subsec:generality}) on a clean Windows XP system. Next, we obtained the Hacker Defender \cite{hxdef} rootkit, which (among many other stealth techniques) hides processes by injecting into all processes on the system and hooking API calls using inline code modification. We configured it to hide any process named \texttt{rcmd.exe}. We then renamed a copy of Notepad to \texttt{rcmd.exe}, and infected the system. After infection, we checked that \texttt{rcmd.exe} was no longer visible from the Task Manager. We then ran our generated introspection program, and found that it successfully listed both our hidden process and the rootkit's own userland component (\texttt{hxdef100.exe}).

Beyond verifying that our program is immune to existing rootkits that alter kernel code, we also need to consider the possibility that someone might actively attempt to evade our introspections. First, because Virtuoso attempts to faithfully replicate the functioning of actual system APIs, vulnerabilities that allow attackers to evade the standard APIs will be reflected in the generated introspection programs. Data-only attacks (i.e. Direct Kernel Object Manipulation, or DKOM) also poses a challenge to our system, because in this case even uncompromised OS APIs will report incorrect results. For example, the standard Windows process listing API can be evaded by rootkits that directly manipulate the process data structure to unlink a malicious process, hiding it from the \texttt{EnumProcesses} API. Careful combinations of existing introspections can still thwart this attack, however: by calling \textbf{getpid} every time the \texttt{CR3} register changes and comparing this list against the one returned by \textbf{pslist}, hidden processes can still be detected. Further research is necessary to see if such defenses can be found for other kinds of data-only attack.

Second, an attacker may attempt to take advantage of the fact that Virtuoso uses dynamic analysis to cause it to malfunction. Specifically, he may attempt to set the \emph{free variables} used by the program in such a way as to cause some conditional branch that does not have full coverage to be exercised. The opportunity for evasion afforded by this is minimal: if an attacker does find a way to reliably manipulate the system into a state that causes an introspection program to fail, the fix can be generated quickly, as the attack could be used for a new training run that would update the existing introspection program. Extra care must be exercised in this case, however: if the attack also alters code that the introspection depends upon, updating the introspection program could cause malicious code to be incorporated into the generated program. Kernel code integrity checks (i.e., verifying that the code seen in training matches that found in an uncompromised version of the OS) would help in this scenario, but can be difficult to implement in practice, and would require more domain knowledge than we currently assume.

Additionally, we note that our dynamic slicing method allows us to quantify precisely the set of global kernel data that a given introspection depends on (note that data from userland is read from the training data, and is not available to the attacker for manipulation). Even within this set of kernel data, only a subset may be susceptible to attacker manipulation: as Dolan-Gavitt et al.~\cite{Dolan-Gavitt:2009} describe, arbitrary modification of global kernel data can cause system instability. Ultimately, this problem is best resolved by improving training, and we hope to explore automated means of improving coverage in future work.

The general problem of defending against malware that is VMI-aware, and has the ability to tamper with kernel data structure layouts and algorithms, remains open. Such malware, which would affect most current VMI solutions, has been discussed in other work \cite{Bahram:2010}, and defending against it is a difficult problem. Although we consider this problem out of scope for Virtuoso, we hope to explore more general defenses in future work.

Finally, if a vulnerability is found in the APIs that support the introspection, an attacker may attempt to compromise the security of the monitor by causing it to execute malicious code. Because the runtime environment has no facility for translating new code, it is effectively a Harvard architecture, so standard code injection techniques are not possible against programs generated by Virtuoso. However, in recent years a new technique, \emph{return-oriented programming} \cite{Shacham:2007}, has demonstrated that malicious computation can be performed by chaining together snippets of code linked by dynamic jumps (e.g., returns), and subsequent work has shown that this allows for exploitation of Harvard architecture devices \cite{Checkoway:2009}. Despite these developments, we believe that the relatively small size of our programs (the largest in our test set has only 4030 basic blocks, and only 140 of these contain a dynamic jump), combined with the fact that the runtime environment executes code at the granularity of a basic block (i.e., it is not possible to execute only part of a block), will make it impossible to construct the necessary set of gadgets. More work, however, is necessary to prove this intuition definitively.

\subsection{Performance}
\label{sec:results:subsec:performance}

\begin{figure}[t!]
\begin{centering}
\input{virtuoso-performance.tab}
\caption[Runtime performance of generated programs]{Runtime performance of generated programs. Times given are in milliseconds, and are averaged over 100 runs.}
\label{performance}
\end{centering}
\end{figure}

Performance results for the programs described in Section
\ref{sec:results:subsec:generality} are shown in Figure
\ref{performance}. The tests were carried out on a
Intel\textregistered~Core\texttrademark~2 Quad 2.4 GHz CPU machine with
4 gigabytes of RAM running Debian/GNU Linux unstable (kernel
2.6.32-amd64). Each test was run 100 times, and the results were
averaged.

Of the three operating systems shown, the results for Linux stand out as
being particularly slow. We investigated this result further and
found that the overhead was caused by the interface Linux uses to
retrieve system information. In contrast to Haiku and Windows, which
have specific system calls to inspect the state of the system, Linux
exposes these details through the \texttt{/proc} filesystem. This means
that the code that implements tools such as \texttt{pslist} needs to
open files, list directories, and so on, all of which results in much
more code being executed than on Windows and Haiku.

Although the times shown are not currently fast enough to enable online
monitoring, we stress that our current implementation is unoptimized
and translates the x86 code to Python, which it then runs in an
environment that is also written in Python. One performance improvement
would be to port the runtime to C and generate x86 binary code that can
be run on the host (after transforming potentially dangerous
instructions into safe equivalents, redirecting memory loads and stores
to the guest VM, and so on). We expect that this would provide
performance at least on par with QEMU, which uses similar techniques in
its whole-system emulation. It is also worth noting that even
unoptimized, the programs generated by Virtuoso are fast enough for use
in forensic analysis of memory dumps.
