\section {Overview}
\label{overview}

\begin{figure}[t!]
\centering
\includegraphics[width=3in]{dssig-figures/eproc_sig.pdf}
\caption[A na\"ive signature for the \texttt{EPROCESS} data structure]{A na\"ive signature for the \texttt{EPROCESS} data structure. The constraints shown are a subset of those used in PTFinder's process signature. Because the \texttt{Size} field is not used by the operating system, an attacker can change its value, hiding the process from a scanner using this signature.}
\label{eproc_sig}
\end{figure}

\begin{figure}[t!]
\centering
\includegraphics[width=3.25in]{dssig-figures/dkom_evasion.pdf}
\caption[A portion of the process list while a process hiding attack is underway]{A portion of the process list while a process hiding attack is underway. The hidden process has been removed from the doubly linked list, and its \texttt{Size} field has been changed to evade the signature above.}
\label{dkom_evasion}
\end{figure}

A signature-based scanner examines each offset in physical memory, looking for predefined patterns in the data that indicate the presence of a particular data structure. These patterns take the form of a set of \emph{constraints} on the values of various fields in the structure. For example, Figure \ref{eproc_sig} shows a simple signature for a process data structure in Windows (called \texttt{EPROCESS}; this structure holds accounting information and metadata about a task). The constraints check to see that the \texttt{Type} and \texttt{Size} fields match predefined constants, that the \texttt{ThreadListHead} pointer is greater than a certain value, and that the \texttt{DirectoryTableBase} is aligned to 0x20 bytes. These invariants must be general enough to encompass all instances of the data structure, but specific enough to avoid matching random data in memory.

\label{threat}
An adversary's goal is to hide the existence of a kernel data structure from a signature-based scanner while continuing to make use of that data structure. We assume that the attacker has the ability to run code in kernel-mode, can read and modify any kernel data, and cannot alter existing kernel code. This threat model represents a realistic attacker: it is increasingly common for malware to gain the ability to execute code in kernel mode \cite{F-Secure:2008jt}, and there are a number of solutions available that can detect and prevent modifications to the core kernel code \cite{Petroni:2007hb,Seshadri:2007kk}, but we are not aware of any solutions that protect kernel data from malicious modification.

To carry out a process hiding attack, such as the one shown in Figure \ref{dkom_evasion}, an attacker conceals the process from the operating system using a technique such as DKOM. This attack works by removing the kernel data structure (\texttt{EPROCESS}) representing that process from the OS's list of running processes. The process continues to run, as its threads are known to the scheduler, but it will not be visible to the user in the task manager. However, it will still be visible to a signature-based scanner \cite{Schuster:2006eb,Betz:nx} that searches kernel memory for process structures rather than walking the OS process list.

To evade such scanners, the attacker must modify one of the fields in the process data structure so that some constraint used by the signature no longer holds. The field must also be carefully chosen so that the modification does not cause the OS to crash or the malicious program to stop working. In the example shown in Figure \ref{dkom_evasion}, the attacker zeroes the \texttt{Size} field, which has no effect on the execution of his malicious process, but which effectively hides the data structure from the scanner.

In order to defend against these kinds of attacks, signatures for data structures must be based on invariants that the attacker cannot violate without crashing the OS or causing the malicious program to stop working. The signature's constraints, then, should be placed only on those fields of the data structure that are critical to the correct operation of the operating system. Rather than relying on human judgement, which is prone to errors, our solution profiles OS execution in order to determine the most frequently accessed fields, and then actively tries to modify their contents to determine which are critical to the correct functioning of the system. Such fields will be difficult for an attacker to modify without crashing the system, and are good candidates for robust signatures.

Finally, we will demonstrate that it is possible to automatically infer invariants on these robust fields and construct a scanner that is resistant to evasion attacks.
