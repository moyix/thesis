\section{Introspection for Security}
\label{sec:usage}

\begin{figure*}[t!]
\centering
\includegraphics[width=5in]{virtuoso-figures/combined.pdf}
\caption[An example of Virtuoso's usage]{An example of Virtuoso's usage. A small in-guest
\emph{training program} that retrieves the name of a process given its
PID is executed in the training environment a number of times, producing
several instruction traces. These traces are then analyzed and merged to
create an introspection program that can be used to securely introspect
on the state of a guest virtual machine from outside the VM. We explain
further details of this diagram in Section \ref{sec:design}.}
\label{running_example}
\end{figure*}

To motivate the design of Virtuoso, we consider the common scenario
where \emph{secure introspection} into the state of a running virtual
machine (known as the \emph{guest}) from outside that VM (i.e., from
the \emph{host}) is needed. This need may arise when performing
out-of-the-box malware analysis or debugging\cite{jiang:2007,
joshi:2005a}, or when attempting to secure a commodity operating
system by placing security software in an isolated virtual machine for
intrusion detection \cite{garfinkel:2003b} or active monitoring
\cite{payne:2008}. In cases such as these it is easy to see why
in-guest monitoring is undesirable: if the guest OS can be
compromised, then the in-guest tools themselves can become corrupted
or the system APIs can be altered to return false information (such as
by omitting a malicious process from a task list).

Developing tools to perform such introspections, however, is no easy
task: extracting even simple information such as the current process ID
or a list of active network connections requires deep understanding of
the operating system running in the VM. In particular, the
\emph{location} of the data, its \emph{layout}, and what
\emph{algorithms} should be used to read it must all be known. While
some recent work has addressed the problem of automatically reverse
engineering data structure layouts \cite{Lin:2010, Slowinska:2011,
Lee:2011}, the overall task of creating introspections is still largely
a manual effort. With Virtuoso, however, another option is available: by
simply writing small programs (which we refer to as \emph{training
programs}) for the guest OS that extract the desired information,
host-level introspection tools can be \emph{automatically} created that
later retrieve the same information at runtime from outside the running
VM.

To illustrate how Virtuoso can be used to quickly create secure
introspection programs, consider the running example shown in Figure
\ref{running_example}. Here, a developer wishes to create an
introspection that obtains the name of a process by its PID. To do so,
he first writes an in-guest program that queries the OS's APIs for the
information. Such programs are generally trivial to create for even
modestly skilled programmers. The program is then run inside Virtuoso,
which records multiple executions, analyzes the resulting instruction
traces, and creates an out-of-guest introspection program that can
retrieve the name of a process running inside a virtual machine. This
program can then be deployed to a secure virtual machine and used to
obtain this security-relevant information at runtime. The generated
program does not call any in-guest APIs, but rather contains all the
low-level instructions necessary to implement that API outside of the
virtual machine. This entire procedure involves only minimal effort on
the part of the developer, and requires no reverse engineering or
specialized knowledge of the OS's internals.

Tools generated in this way are specific to the operating system for
which the corresponding training programs were developed; that is, if
the developer writes a program that gets the current process ID in
Windows XP, the generated out-of-the-box tool will only work for Windows
XP guests. However, supporting a new platform only requires adapting the
training program to a new OS and its API, rather than costly reverse
engineering or source code analysis by a specialist. Moreover, different
releases of various operating systems are often backwards compatible at
either the source or binary level. This is the case, for example, with
Microsoft Windows: programs written for Windows NT 3.1 (released over 15
years ago) can run without modification on Windows 7. In this case
Virtuoso could analyze the behavior of the same program under each
release of Windows, generating a suite of host-level introspection tools
to cover any version desired with no additional programmer effort.
Changes in the underlying implementations of the OS APIs would be
automatically reflected in the generated host introspection tools.

Once the introspection tools have been generated for a particular OS
release, they can be deployed to aid in the secure monitoring of any
number of virtual machines running that OS. The generated introspection
tools will provide accurate data about the current state of the guest
VM, even when the code of that VM is compromised.\footnote{As with all
introspection, malicious alterations to the guest's kernel data structure
layouts, locations, or algorithms may still cause incorrect results to
be reported.} Thus, Virtuoso produces tools that enable secure
monitoring of insecure commodity operating systems, and accomplishes
this task with only minimal human intervention.

\subsection{Scope and Assumptions}

Although we believe Virtuoso represents a large step forward in
narrowing the semantic gap, there are some fundamental limitations to
our techniques and constructs that Virtuoso is not currently equipped to
handle. First, Virtuoso is, by design, only able to produce
introspection code for data that is already accessible via some system
API in the guest. These introspections are a subset of those that could be
programmed manually, and it is possible that some security-relevant
data are not exposed through system APIs. For those functions that are
exposed through system APIs, Virtuoso greatly reduces the cost of
producing introspections; we describe six such useful introspections in
Section \ref{sec:results}.

Second, there are certain code constructs that are difficult to
reproduce, such as synchronization primitives (which may require other
threads to act before an introspection can proceed) and interprocess
communication (IPC). These limitations are not neccessarily fundamental,
but they require additional research to overcome. We discuss these
limitations further in Section \ref{sec:future}.

Finally, if an introspection requires interaction with a hardware
device, such as the disk or a network interface, the introspection
cannot currently be generated by Virtuoso. In Section \ref{sec:future}
we discuss possible extensions to Virtuoso that could enable automatic
generation of some such introspections, however, for some devices any
interaction might irreversibly perturb the guest state (for example, if
an introspection must access the network, there is no way to reverse its
effects---the packets cannot be ``unsent''). This would leave the guest
in an inconsistent state and may cause the system to become unstable; it
also violates the principle that introspection should not alter the
guest.
